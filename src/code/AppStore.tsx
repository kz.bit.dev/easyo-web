import React from 'react'
import {action, observable, reaction, runInAction, when} from 'mobx'
import page from 'page'
import Login from '~/code/pages/auth/components/Login/Login'
import LoginStore from '~/code/pages/auth/components/Login/LoginStore'
import { AuthLayout } from '~/code/components';
import { AuthStore } from '~/code/pages/auth/AuthStore';
import { MainLayout } from '~/code/components/MainLayout';
import { MainLayoutStore } from '~/code/components/MainLayout/MainLayoutStore';
import Users from '~/code/pages/Users/Users';
import { UsersStore } from '~/code/pages/Users/UsersStore';
import { Meals } from "~/code/pages/Meals";
import { MealsStore } from "~/code/pages/Meals/MealsStore";
import {TablesStore} from "~/code/pages/Tables/TablesStore";
import {Tables} from "~/code/pages/Tables";
import {Menus} from "~/code/pages/Menus";
import {MenusStore} from "~/code/pages/Menus/MenusStore";
import {OrdersStore} from "~/code/pages/Orders/OrdersStore";
import {Orders} from "~/code/pages/Orders";
import {OrderPersonal} from "~/code/pages/OrderPersonal";
import {OrderPersonalStore} from "~/code/pages/OrderPersonal/OrderPersonalStore";
import {Dashboard, DashboardStore} from "~/code/pages/Dashboard";

class AppStore {

    constructor() {
        this.initialize();
    }

    @observable
    public ready: boolean = false;

    @observable
    public currentPageTitle: string;

    @observable
    public mainLayoutStore: MainLayoutStore;

    @observable
    public dashboardStore: DashboardStore;

    @observable
    public usersStore: UsersStore;

    @observable
    public mealsStore: MealsStore;

    @observable
    public tablesStore: TablesStore;

    @observable
    public menusStore: MenusStore;

    @observable
    public ordersStore: OrdersStore;

    @observable
    public currentPage: () => JSX.Element = () => null;

    @action
    public setCurrentPage(currentPage: () => JSX.Element) {
        this.currentPage = currentPage;
    }

    private authorize(route: () => void) {
        if (AuthStore.isAuthenticated) return route();

        this.redirectToLogin()
    }

    @observable
    private async initialize() {

        when(() => AuthStore.initialized,

            async () => {
                if (AuthStore.isAuthenticated) {
                    runInAction(() => this.ready = true)
                } else {
                    this.ready = true
                }

                reaction(
                    () => AuthStore.isAuthenticated,
                    isAuthenticated => {
                        if (!isAuthenticated) {
                            this.redirectToLogin()
                        }
                    }
                )
            })
    }

    private redirectToLogin() {
        page('/login')
    }

    @action
    public onLogin() {
        page('/dashboard')
    }

    @action
    public redirectToDashboard() {
        this.authorize(() => {
            page('/dashboard')
        })
    }

    @action
    showDashbordPage() {
        const dashboardStore = this.dashboardStore || (this.dashboardStore = new DashboardStore(this));
        this.wrapAdminLayout(<Dashboard store={ dashboardStore } />, 'DASHBOARD')


    }

    @action
    showUsersPage() {
        const usersStore = this.usersStore || (this.usersStore = new UsersStore(this));
        this.wrapAdminLayout(<Users store={ usersStore }/>, 'USERS')
    }

    @action
    showMealsPage() {
        const mealsStore = this.mealsStore || (this.mealsStore = new MealsStore(this));
        this.wrapAdminLayout(<Meals store={ mealsStore }/>, 'MEALS')
    }

    @action
    showTablesPage() {
        const tablesStore = this.tablesStore || (this.tablesStore = new TablesStore(this));
        this.wrapAdminLayout(<Tables store={ tablesStore }/>, 'TABLES')
    }

    @action
    showMenusPage() {
        const menusStore = this.menusStore || (this.menusStore = new MenusStore(this));
        this.wrapAdminLayout(<Menus store={ menusStore }/>, 'MENUS')
    }

    @action
    showOrdersPage() {
        const ordersStore = this.ordersStore || (this.ordersStore = new OrdersStore(this));
        this.wrapAdminLayout(<Orders store={ ordersStore }/>, 'ORDERS')
    }

    @action
    showOrdersPersonalPage(id?) {
        const orderPersonalStore = new OrderPersonalStore(this, id);
        this.wrapAdminLayout(<OrderPersonal store={ orderPersonalStore }/>, id ? `ORDER #${ id }` : 'Create a new order' )
    }

    @action
    public showLogin() {
        const loginStore = new LoginStore()
        this.wrapAuthLayout( <Login store={ loginStore } /> )
    }

    @action
    public wrapAdminLayout(component: JSX.Element, title?: string) {
        this.authorize(() => {
            const mainLayoutStore = this.mainLayoutStore || (this.mainLayoutStore = new MainLayoutStore(this));
            this.setCurrentPage(() => (
                <MainLayout mainLayoutStore={ mainLayoutStore } title={ title }>
                    { component }
                </MainLayout>
            ))
        });
    }

    @action
    public wrapAuthLayout(component: JSX.Element) {
        this.setCurrentPage(() => (
            <AuthLayout>
                { component }
            </AuthLayout>
        ))
    }

}

const AppStoreInstance = new AppStore()

export { AppStoreInstance as AppStore }
