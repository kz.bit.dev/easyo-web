export default {
    api: {
        baseUrl: ''
    },
    auth: {
        TOKEN: 'token',
        USER_INFO: 'user_info'
    }
}
