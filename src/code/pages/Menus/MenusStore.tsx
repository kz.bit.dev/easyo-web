import React from "react";
import {action, observable, reaction} from "mobx";
import { Button, message } from "antd";
import {fetchAllMenus, fetchDeleteMenu, getMenuColumns} from "~/code/pages/Menus/services";
import { MenuPersonalModalStore } from "./components/MenuPersonalModal/MenuPersonalModalStore";
import styles from './Menus.scss'

export class MenusStore {
    public menuPersonalModalStore;
    private parentStore;

    constructor(parentStore) {
        this.parentStore = parentStore
        this.menuPersonalModalStore = new MenuPersonalModalStore(this);
        this.init()
        reaction(() => this.currentPage, () => {
            this.loadMenus()
        })
    }

    @observable
    public loading: boolean = false;

    @observable
    public menus: any[] = [];

    @observable
    public total: number = 1;

    @observable
    public currentPage: number = 0;

    @action
    init() {
        this.loadMenus()
    }

    @action
    onPageChange(value) {
        this.currentPage = value - 1
    }

    @action
    async loadMenus() {
        this.loading = true;
        const result = await fetchAllMenus({ currentPage: this.currentPage, size: 10 });
        this.loading = false;
        if(result.error) {
            message.error('Request failed! Can\'t load menus ')
        }

        this.menus = result.value.data;
        this.total = result.value.total;

        if(this.menus.length === 0 && this.currentPage !== 0) {
            this.currentPage = 0
        }

    }

    @action
    refreshPage() {
        this.loadMenus()
    }

    @action
    createMenu() {
        this.menuPersonalModalStore.showModal();
    }

    @action
    onEditMenu(menu) {
        this.menuPersonalModalStore.showModal();
        this.menuPersonalModalStore.setMenu(menu)
    }

    @action
    async onDeleteMenu(menu) {
        const result = await fetchDeleteMenu(menu.id)
        if(!result.error) {
            message.success(' Menu deleted');
            this.refreshPage();
            return;
        }
        message.error(result.error.message);
    }

    getGridColumns() {
        const columns = getMenuColumns();
        columns.push(
            {
                title: 'Actions',
                field: 'actions',
                render: (text, menu) => {
                    return (
                        <>
                            <Button onClick={ () => this.onEditMenu(menu) }>
                                EDIT
                            </Button>
                            <Button onClick={ () => this.onDeleteMenu(menu) } className={ styles.deleteButton }>
                                DELETE
                            </Button>
                        </>
                    )
                }
            }
        )
        return columns;
    }
}
