import React from 'react';
import { observer } from "mobx-react";
import {Button, Grid} from "~/code/components";
import { MenusStore } from "./MenusStore";
import { MenuColumn } from "./models";
import { Column}  from "~/code/components/Grid";
import { MenuPersonalModal } from "./components";
import styles from './Menus.scss'

export default observer(({ store }: { store: MenusStore }) => {
    return (
        <div className={ styles.Menus }>
            <div className={ styles.createMenuButton }>
                <Button onClick={ () => store.createMenu() }> Create menu </Button>
            </div>
            <Grid<MenuColumn>
                columns={ store.getGridColumns() as Column<MenuColumn>[]}
                data={ store.menus }
                loading={ store.loading }
                rowKey={ 'id' as keyof MenuColumn }
                pagination={{
                    position: 'both',
                    total: store.total,
                    defaultPageSize: 10,
                    current: store.currentPage + 1,
                    onChange: (v) => store.onPageChange(v)
                }}
                scroll={ { x: true } }
            />
            { store.menuPersonalModalStore.isVisible && (
                <MenuPersonalModal store={ store.menuPersonalModalStore } />
            ) }

        </div>
    )
})
