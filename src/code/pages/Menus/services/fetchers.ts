import {apiCheck, noThrow} from "back-connector";
import {delWithToken, getWithToken} from "~/code/services/fetchers";

export async function fetchAllMenus({ currentPage, size }: { currentPage: number, size: number }) {
    return noThrow<any>(apiCheck(getWithToken('/api/menus',  { page: currentPage, size })))// Todo: change
}

export async function fetchDeleteMenu(id: string) {
    return noThrow(apiCheck(delWithToken('/api/menus/' + id)))
}
