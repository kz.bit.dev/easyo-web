import React from "react";
import { MenuColumn } from "~/code/pages/Menus/models/MenuColumn";

export function getMenuColumns(): MenuColumn [] {
    return [
        {
            title: 'ID',
            field: 'id'
        },
        {
            title: 'Name',
            field: 'menuType'
        },
        {
            title: 'Start date',
            field: 'fromDate'
        },
        {
            title: 'Finish date',
            field: 'toDate'
        }
    ]
}
