export interface Menu {
    id?: string,
    label: string,
    fromDate: string,
    toDate: string
}
