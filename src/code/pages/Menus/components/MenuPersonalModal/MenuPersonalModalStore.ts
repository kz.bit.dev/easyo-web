import {
    action, computed,
    observable,
    runInAction
} from "mobx";
import moment, {Moment} from "moment";
import { message } from "antd";
import FieldStore from "~/code/services/field-store";
import { required } from "~/code/services/validators";
import { isValidArr } from "~/code/services/field-store/services";
import { createMenu, editMenu } from "./services";


export class MenuPersonalModalStore {
    private parentStore;
    constructor(parentStore) {
        this.parentStore = parentStore;
        this.toDate.set(moment().add(1, 'year'));
    }
    public label = new FieldStore<string>([ required(() => 'Please, enter label') ]);
    public toDate = new FieldStore<Moment>([ required(() => 'Please, enter finish date') ]);

    @observable
    public fromDate = moment();

    @observable
    public isVisible: boolean = false;

    @observable
    public id: string = '';

    @observable
    public fileList: any[] = [];

    @observable
    public isSending: boolean = false;


    @action
    onImageLoaded(file?: { path: string }) {
        this.fileList =
            file ? [{
                uid: '-1',
                name: 'image.png',
                status: 'done',
                url: file.path
            }] : []
    }

    @action
    setFinishDate(v) {
        this.toDate.set(v);
    }

    @action
    showModal() {
        this.isVisible = true
    }

    @computed
    public get isEditMode() {
        return !!this.id
    }

    @action
    closeModal() {
        this.isVisible = false;
        this.label.set('');
        this.id = null;
        this.fileList = [];
    }

    @action
    async save() {
        if (!isValidArr(this.label, this.toDate)) return;
        const menu = {
            label: this.label.value,
            fromDate: moment(this.fromDate).format("YYYY-MM-DD"),
            toDate: moment(this.toDate.value).format("YYYY-MM-DD"),
            menuType: this.label.value, // todo: change,
            ...( this.fileList && this.fileList.length > 0 ? { imageUrl: this.fileList[0].url }: {})
        };

        this.isSending = true;

        const result = this.isEditMode ? await editMenu(this.id, menu) : await createMenu(menu);

        if (!result.error) {
            this.isSending = false;
            message.success(this.isEditMode ? 'Menu edited' : 'Menu created');
            this.closeModal();
            this.parentStore.refreshPage();
            return;
        }

        runInAction(() => {
            if (!result.error) return;
            this.isSending = false;
            message.error(result.error.message)
        })
    }

    @action
    setMenu(menu) {
        this.id = menu.id;
        this.label.set(menu.menuType);
        this.fileList = menu.imageUrl ?
            [{
                uid: '-1',
                name: 'image.png',
                status: 'done',
                url: menu.imageUrl
            }]:
            [];
    }
}
