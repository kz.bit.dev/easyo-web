import { Menu } from "././../models";
import { postWithToken, putWithToken } from "~/code/services/fetchers";
import {apiCheck, noThrow } from "back-connector";

export function createMenu(menu: Menu) {
    return noThrow(apiCheck(postWithToken('/api/menus', menu)))
}

export function editMenu(id: string, menu: Menu) {
    return noThrow(apiCheck(putWithToken('/api/menus/' + id, menu)))
}
