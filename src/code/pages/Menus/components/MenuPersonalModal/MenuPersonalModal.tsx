import React from "react";
import { observer } from "mobx-react";
import {Icon, Modal, Upload} from "antd";
import { MenuPersonalModalStore } from "./MenuPersonalModalStore";
import {Button, DatePicker, InputItem} from "~/code/components";
import Label from "~/code/components/Label/Label";
import styles from './MenuPersonalModal.scss'
import { uploadFile } from "~/code/services/fetchers";

export default observer(({ store }: { store: MenuPersonalModalStore }) => {
    return (
        <div className={ styles.MenuPersonalModal }>
            <Modal
                title={ store.id ? `Menu #${ store.id }` : 'Create a new menu' }
                onCancel={ () => store.closeModal() }
                visible={ store.isVisible }
                onOk={ () => store.save() }
                okText={ 'Save' }
                footer={[
                    <Button key="back" onClick={ () => store.closeModal() }>
                        Back
                    </Button>,
                    <Button key="submit" type="primary" loading={ store.isSending } onClick={ () => store.save() }>
                        Save
                    </Button>
                ]}
            >

                <Upload
                    listType="picture-card"
                    fileList={ store.fileList }
                    onRemove={ () => store.onImageLoaded(null) }
                    customRequest={ async ({ onSuccess, onError, file }) => {
                        const result = await uploadFile(file);
                        if(!result.error) {
                            store.onImageLoaded(result.value);
                            return onSuccess(null, file);
                        }
                        onError(result.error)
                    }}
                >
                    <div>
                        <Icon type={ 'plus'} />
                        <div className="ant-upload-text">Upload</div>
                    </div>
                </Upload>

                <InputItem
                    label={ 'Label' }
                    store={ store.label }
                    placeholder={ 'Label' }
                    inputProps={{
                        name: 'label_field',
                        disabled: store.isSending
                    }}
                />

                <Label> Finish date </Label>
                <DatePicker
                    disabledDate={ (current => {
                       return current.isBefore(store.fromDate)
                    })}
                    store={ store.toDate }
                    onChange={ (e) => store.setFinishDate(e) }
                />
            </Modal>
        </div>
    )
})
