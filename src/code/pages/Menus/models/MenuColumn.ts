export interface MenuColumn {
    title: string,
    field: string,
    render?: (text: any, record: any) => any
}
