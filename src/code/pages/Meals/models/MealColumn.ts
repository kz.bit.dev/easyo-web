export interface MealColumn {
    title: string,
    field?: string,
    render?: (text: any, record: any) => any
}
