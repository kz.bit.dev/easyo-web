import {apiCheck, noThrow} from "back-connector";
import {delWithToken, getWithToken} from "~/code/services/fetchers";

export async function fetchAllMeals({ currentPage, size, filter }: { currentPage: number, size?: number, filter?: any }) {
    return noThrow<any>(apiCheck(getWithToken('/api/meals',  { page: currentPage, ...(size ? { size } : {}), ...(filter ? filter : {}) })))
}

export async function fetchDeleteMeal(id: string) {
    return noThrow(apiCheck(delWithToken('/api/meals/' + id)))
}
