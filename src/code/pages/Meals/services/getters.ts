import React from "react";
import { MealColumn } from "~/code/pages/Meals/models/MealColumn";

export function getMealColumns(menus): MealColumn [] {
    return [
        {
            title: 'ID',
            field: 'id'
        },
        {
            title: 'Name',
            field: 'name'
        },
        {
            title: 'Menu',
            render: (text, record) => {
                const item = menus.find(el => el.value === record.menu.id)
                return item && item.label
            }
        },
        {
            title: 'Unit price',
            field: 'unitPrice'
        },
        {
            title: 'Size',
            field: 'size'
        }
    ]
}
