import React from "react";
import {action, observable, reaction} from "mobx";
import { Button, message } from "antd";
import {fetchAllMeals, fetchDeleteMeal, getMealColumns} from "~/code/pages/Meals/services";
import { MealPersonalModalStore } from "./components/MealPersonalModal/MealPersonalModalStore";
import styles from './Meals.scss'
import {getMenuList} from "~/code/pages/Meals/components/MealPersonalModal/services";

export class MealsStore {
    public mealPersonalModalStore;
    private parentStore;

    constructor(parentStore) {
        this.parentStore = parentStore
        this.mealPersonalModalStore = new MealPersonalModalStore(this);
        this.init()
        reaction(() => ({
            currentPage: this.currentPage,
            filter: this.filter
        }), () => {
            this.loadMeals()
        })
    }

    @observable
    public menus = []

    @observable
    public filter: any = {};

    @observable
    public isLoading: boolean = false;

    @observable
    public Meals: any[] = [];

    @observable
    public total: number = 1;

    @observable
    public currentPage: number = 0;

    @action
    async init() {
        this.loadMeals()
        this.menus = await getMenuList()
    }

    @action
    onPageChange(value) {
        this.currentPage = value - 1
    }

    @action
    onFilterChange(name, value) {
        this.filter = {
            ...this.filter,
            [name]: value
        }
    }

    @action
    async loadMeals() {
        this.isLoading = true;
        const result = await fetchAllMeals({ currentPage: this.currentPage, size: 10, filter: this.filter });
        this.isLoading = false;
        if(result.error) {
            message.error('Request failed! Can\'t load Meals ')
        }

        this.Meals = result.value.data;
        this.total = result.value.total;

        if(this.Meals.length === 0 && this.currentPage !== 0) {
            this.currentPage = 0
        }

    }

    @action
    refreshPage() {
        this.loadMeals()
    }

    @action
    createMeal() {
        this.mealPersonalModalStore.showModal();
    }

    @action
    onEditMeal(meal) {
        this.mealPersonalModalStore.showModal();
        this.mealPersonalModalStore.setMeal(meal)
    }

    @action
    async onDeleteMeal(meal) {
        const result = await fetchDeleteMeal(meal.id)
        if(!result.error) {
            message.success('Meal deleted');
            this.refreshPage();
            return;
        }
        message.error(result.error.message);
    }

    getGridColumns() {
        const columns = getMealColumns(this.menus);
        columns.push(
            {
                title: 'Actions',
                field: 'actions',
                render: (text, meal) => {
                    return (
                        <>
                            <Button onClick={ () => this.onEditMeal(meal) }>
                                EDIT
                            </Button>
                            <Button onClick={ () => this.onDeleteMeal(meal) } className={ styles.deleteButton }>
                                DELETE
                            </Button>
                        </>
                    )
                }
            }
        );
        return columns;
    }
}
