import React from 'react';
import { observer } from "mobx-react";
import { Button, Grid } from "~/code/components";
import { MealsStore } from "./MealsStore";
import { MealColumn } from "./models";
import { Column}  from "~/code/components/Grid";
import { MealPersonalModal } from "./components/MealPersonalModal";
import styles from './Meals.scss'
import { Select } from "antd";
const { Option } = Select;

export default observer(({ store }: { store: MealsStore }) => {
    return (
        <div className={ styles.Meals }>
            <div>
                <div>
                    <h4>
                        Menu
                    </h4>
                    <Select
                        style={{
                            width: '200px'
                        }}
                        value={ store.filter ? store.filter.menu : '' }
                        showSearch={ true }
                        optionFilterProp="children"
                        onChange={ (v) => store.onFilterChange('menu', v) }
                    >
                        {
                            store.menus.map(({ label, value  }) => (
                                <Option key={ value } value={ value }>{ label }</Option>
                            ))
                        }
                    </Select>
                </div>
            </div>
            <div className={ styles.createMealButton }>
                <Button onClick={ () => store.createMeal() }> Create meal </Button>
            </div>
            <Grid<MealColumn>
                columns={ store.getGridColumns() as Column<MealColumn>[]}
                data={ store.Meals }
                loading={ store.isLoading }
                rowKey={ 'id' as keyof MealColumn }
                pagination={{
                    position: 'both',
                    total: store.total,
                    defaultPageSize: 10,
                    current: store.currentPage + 1,
                    onChange: (v) => store.onPageChange(v)
                }}
                scroll={ { x: true } }
            />
            { store.mealPersonalModalStore.isVisible && (
                    <MealPersonalModal
                        store={ store.mealPersonalModalStore }
                    />
                )
            }

        </div>
    )
})
