import { Meal } from "././../models";
import {getWithToken, postWithToken, putWithToken} from "~/code/services/fetchers";
import {apiCheck, noThrow} from "back-connector";
import {message} from "antd";

export function createMeal(meal: Meal) {
    return noThrow(apiCheck(postWithToken('/api/meals', meal)))
}

export function editMeal(id: string, meal: Meal) {
    return noThrow(apiCheck(putWithToken('/api/meals/' + id, meal)))
}

export async function getMenuList(): Promise<any[]> {
    const result = await noThrow(apiCheck<{ data: any }>(getWithToken('/api/menus')))
    if(result.error) {
        message.error(result.error.message)
        return [];
    }

    return result.value.data.map((el) => ({
        label: el.menuType, // todo: change
        value: el.id
    }))
}
