import {
    action, computed,
    observable,
    runInAction
} from "mobx";
import { message } from "antd";
import FieldStore from "~/code/services/field-store";
import { required } from "~/code/services/validators";
import { isValidArr } from "~/code/services/field-store/services";
import { createMeal, editMeal, getMenuList } from "./services";

export class MealPersonalModalStore {
    private parentStore;
    constructor(parentStore) {
        this.parentStore = parentStore;
        this.init();
    }
    public name;
    public description;
    public size;
    public unitPrice;
    public menu;

    @action
    init() {
        this.name = new FieldStore<string>([ required(() => 'Please, enter name') ]);
        this.description = new FieldStore<string>([ required(() => 'Please, enter description') ]);
        this.size = new FieldStore<string>([ required(() => 'Please, enter size') ]);
        this.unitPrice = new FieldStore<string>([ required(() => 'Please, enter unit price') ]);
        this.menu = new FieldStore<string>([ required(() => 'Please, enter menu') ]);
    }

    @observable
    public isVisible: boolean = false;

    @observable
    public menuList: any[] = [];

    @observable
    public fileList: any[] = [];

    @observable
    public id: string = '';

    @observable
    public isSending: boolean = false;

    @observable
    public isLoading: boolean = false;

    @action
    async showModal() {
        this.isVisible = true;
        this.isLoading = true;
        this.menuList = await getMenuList();
        this.isLoading = false;
    }

    @action
    onMenuChange(v) {
        this.menu.set(v, true)
    }

    @action
    onImageLoaded(file?: { path: string }) {
        this.fileList =
            file ? [{
                uid: '-1',
                name: 'image.png',
                status: 'done',
                url: file.path
            }] : []
    }

    @computed
    public get isEditMode() {
        return !!this.id
    }

    @action
    closeModal() {
        this.isVisible = false;
        this.init()
        this.id = null;
        this.fileList = [];
    }

    @action
    async save() {
        if (!isValidArr(this.name, this.description, this.size, this.unitPrice, this.menu)) return;

        const meal = {
            name: this.name.value,
            description: this.description.value,
            size: this.size.value,
            unitPrice: this.unitPrice.value,
            menu: {
                id: this.menu.value
            },
            ...( this.fileList && this.fileList.length > 0 ? { imageUrl: this.fileList[0].url }: {})
        };

        this.isSending = true;

        const result = this.isEditMode ? await editMeal(this.id, meal) : await createMeal(meal);

        if (!result.error) {
            this.isSending = false;
            message.success(this.isEditMode ? 'Meal edited' : 'Meal created');
            this.closeModal();
            this.parentStore.refreshPage();
            return;
        }

        runInAction(() => {
            if (!result.error) return;
            this.isSending = false;
            message.error(result.error.message)
        })
    }

    @action
    setMeal(meal) {
        this.id = meal.id;
        this.name.set(meal.name);
        this.description.set(meal.description);
        this.size.set(meal.size);
        this.menu.set(meal.menu.id);
        this.unitPrice.set(meal.unitPrice);
        this.fileList = meal.imageUrl ?
            [{
                uid: '-1',
                name: 'image.png',
                status: 'done',
                url: meal.imageUrl
            }]:
            [];
    }
}
