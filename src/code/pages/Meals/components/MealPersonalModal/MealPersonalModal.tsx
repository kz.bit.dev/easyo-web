import React from "react";
import { observer } from "mobx-react";
import { Icon, Modal, Upload } from "antd";
import { MealPersonalModalStore } from "./MealPersonalModalStore";
import { Button, InputItem, Label, Select, Option } from "~/code/components";
import styles from './MealPersonalModal.scss'
import {uploadFile} from "~/code/services/fetchers";

export default observer(({ store }: { store: MealPersonalModalStore }) => {
    return (
        <div className={ styles.MealPersonalModal }>
            <Modal
                title={ store.id ? `Meal #${ store.id }` : 'Create a new meal' }
                onCancel={ () => store.closeModal() }
                visible={ store.isVisible }
                onOk={ () => store.save() }
                okText={ 'Save' }
                footer={[
                    <Button key="back" onClick={ () => store.closeModal() }>
                        Back
                    </Button>,
                    <Button key="submit" type="primary" loading={ store.isSending } onClick={ () => store.save() }>
                        Save
                    </Button>
                ]}
            >
                <Upload
                    listType="picture-card"
                    fileList={ store.fileList }
                    onRemove={ () => store.onImageLoaded(null) }
                    customRequest={ async ({ onSuccess, onError, file }) => {
                        const result = await uploadFile(file);
                        if(!result.error) {
                            store.onImageLoaded(result.value);
                            return onSuccess(null, file);
                        }
                        onError(result.error)
                    }}
                >
                    <div>
                        <Icon type={ 'plus'} />
                        <div className="ant-upload-text">Upload</div>
                    </div>
                </Upload>
                <InputItem
                    label={ 'Name' }
                    store={ store.name }
                    placeholder={ 'Name' }
                    inputProps={{
                        name: 'name_field',
                        disabled: store.isSending
                    }}
                />
                <InputItem
                    label={ 'Description' }
                    store={ store.description }
                    placeholder={ 'Description' }
                    inputProps={{
                        name: 'description_field',
                        disabled: store.isSending
                    }}
                />

                <InputItem
                    isNumeric={ true }
                    label={ 'Size' }
                    store={ store.size }
                    placeholder={ '' }
                    inputProps={{
                        type: 'number',
                        name: 'size_field',
                        disabled: store.isSending
                    }}
                />
                <InputItem
                    isNumeric={ true }
                    label={ 'Unit price' }
                    store={ store.unitPrice }
                    placeholder={ '' }
                    inputProps={{
                        type: 'number',
                        name: 'size_field',
                        disabled: store.isSending
                    }}
                />

                <Label>Menu</Label>

                <Select
                    style={{
                        width: '100%'
                    }}
                    defaultValue={ store.menu.value }
                    disabled={ store.isSending || store.isLoading }
                    loading={ store.isLoading }
                    onChange={ (v) => store.onMenuChange(v) }
                    store={ store.menu }
                >
                    {
                        store.menuList.map(({ label, value  }) => (
                            <Option key={ value } value={ value }>{ label }</Option>
                        ))
                    }
                </Select>
            </Modal>
        </div>
    )
})
