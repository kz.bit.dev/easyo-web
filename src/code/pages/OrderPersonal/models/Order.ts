export interface Order {
    id?: string,
    acceptedUser: {
        id: string
    },
    table: {
        id: string
    },
    orderLineList: any[]
}
