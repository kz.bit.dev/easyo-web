import {
    action, computed,
    observable,
    runInAction, toJS
} from "mobx";
import { message } from "antd";
import FieldStore from "~/code/services/field-store";
import { required } from "~/code/services/validators";
import { isValidArr } from "~/code/services/field-store/services";
import {createOrder, editOrder, fetchOrder} from "./services";
import { fetchAllUsers } from "~/code/pages/Users/services";
import {fetchAllTables} from "~/code/pages/Tables/services";
import {convertArrayToInputFieldStore, convertInputFieldStore} from "~/code/pages/OrderPersonal/services/converters";
import {fetchAllMeals} from "~/code/pages/Meals/services";
import moment from "moment";

export class OrderPersonalStore {
    private parentStore;
    constructor(parentStore, id?: string) {
        this.parentStore = parentStore;
        this.init(id)
    }
    @observable
    public acceptedUser = new FieldStore<string>([ required(() => 'Please, enter a waiter') ]);
    @observable
    public table = new FieldStore<string>([ required(() => 'Please, enter a table') ]);
    @observable
    public orderedDate = new FieldStore<string>([ required(() => 'Please, enter a order date') ]);

    @observable
    public orderLineList = []

    @observable
    public id: string = '';

    @observable
    public users: any[] = [];

    @observable
    public meals: any[] = [];

    @observable
    public statuses: any[] = [
        {
            label: 'PREPARE',
            value: 'PREPARE'
        },
        {
            label: 'AWAITING',
            value: 'AWAITING'
        },
        {
            label: 'DONE',
            value: 'DONE'
        }
    ];

    @observable
    public tables: any[] = [];

    @observable
    public isSending: boolean = false;

    @observable
    public isLoading: boolean = false;

    @action
    async init(id?: string) {
        this.loadUsers()
        this.loadMeals()
        this.loadTables()
        if (id) {
            await this.loadOrder(id)
        }
    }

    @action
    public addNewMeal() {
        this.orderLineList.push(convertInputFieldStore({}))
    }

    @action
    public closeMeal(index) {
        this.orderLineList.splice(index, 1);
    }

    @action
    public async loadOrder(id) {
        this.isLoading = true;
        const result = await fetchOrder(id)
        this.isLoading = false;
        if(!result.error) {
            this.setOrder(result.value)
            return;
        }
        message.error(result.error.message);
    }

    @computed
    public get isEditMode() {
        return !!this.id
    }

    @action
    async loadUsers() {
        this.isLoading = true;
        const result = await fetchAllUsers({ currentPage: 0 });
        this.isLoading = false;
        if(result.error) {
            message.error('Request failed! Can\'t load users ')
        }

        this.users = result.value.data.map((el) => ({
            label: (el.firstName ? el.firstName : '') + ' ' +  (el.lastName ?  el.lastName : ''),
            value: el.id
        }));
    }

    @action
    async loadMeals() {
        this.isLoading = true;
        const result = await fetchAllMeals({ currentPage: 0 });
        this.isLoading = false;
        if(result.error) {
            message.error('Request failed! Can\'t load meals ')
        }

        this.meals = result.value.data.map((el) => ({
            label: el.name,
            value: el.id,
            item: el
        }));
    }

    @action
    async loadTables() {
        this.isLoading = true;
        const result = await fetchAllTables({ currentPage: 0 });
        this.isLoading = false;
        if(result.error) {
            message.error('Request failed! Can\'t load tables ')
        }

        this.tables = result.value.data.map((el) => ({
            label: el.number,
            value: el.id
        }));
    }

    @action
    onTableChange(v) {
        this.table.set(v, true)
    }

    @action
    onUserChange(v) {
        this.acceptedUser.set(v, true)
    }

    @action
    onOrderLineChange(v, name ,index) {
        this.orderLineList[index][name].set(v)
    }

    @action
    onOrderLineMealChange(v ,index) {
        this.orderLineList[index].meal.set({
            id: v
        });

        const selectedMeal = this.meals.find(el => el.value === v)
        if(selectedMeal) {
            this.orderLineList[index].unitPrice.set(selectedMeal.item.unitPrice)
            this.orderLineList[index].quantity.set(selectedMeal.item.quantity)
        }
    }

    @action
    async save() {
        const fieldStores = this.orderLineList.reduce((acc, el) => {
            acc.push(el.acceptedUser)
            acc.push(el.meal)
            acc.push(el.quantity)
            acc.push(el.status)
            acc.push(el.unitPrice)
            return acc;
        }, []);

        if (!isValidArr(this.acceptedUser, this.table, ...fieldStores)) {
            message.error('Error: Please, enter all fields')
            return;
        }

        const orderLineList = this.orderLineList.map((el) => {
            return  {
                ...( el.id ? { id: el.id }: {}),
                acceptedUser: el.acceptedUser.value,
                meal: el.meal.value,
                quantity: el.quantity.value,
                status: el.status.value,
                unitPrice: el.unitPrice.value
            }
        });
        const order = {
            acceptedUser: {
                id: this.acceptedUser.value
            },
            table: {
                id: this.table.value
            },
            orderedDate: this.orderedDate.value || moment().format("YYYY-MM-DDTHH:MM:SS"),
            orderLineList
        };

        this.isSending = true;

        const result = this.isEditMode ? await editOrder(this.id, order) : await createOrder(order);

        if (!result.error) {
            this.isSending = false;
            message.success(this.isEditMode ? 'Order edited' : 'Order created');
            location.href = '/orders'
            return;
        }

        runInAction(() => {
            if (!result.error) return;
            this.isSending = false;
            message.error(result.error.message)
        })
    }

    @action
    setOrder(order) {
        this.id = order.id;
        this.acceptedUser.set(order.acceptedUser.id);
        this.table.set(order.table.id);
        this.orderedDate.set(order.orderedDate);
        this.orderLineList= order.orderLineList ? convertArrayToInputFieldStore(order.orderLineList) : []
    }
}
