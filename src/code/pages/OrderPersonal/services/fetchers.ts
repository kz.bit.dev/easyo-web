import { Order } from "././../models";
import {getWithToken, postWithToken, putWithToken} from "~/code/services/fetchers";
import {apiCheck, noThrow, post} from "back-connector";
import uuid from 'uuid/v4'

export function createOrder(order: Order) {
    return noThrow(apiCheck(postWithToken('/api/orders', order)))
}

export function editOrder(id: string, order: Order) {
    return noThrow(apiCheck(putWithToken('/api/orders/' + id, order)))
}

export function fetchOrder(id: string) {
    return noThrow(apiCheck(getWithToken('/api/orders/' + id)))
}
