import FieldStore from "~/code/services/field-store";
import {positiveNumeric, required} from "~/code/services/validators";

export function convertArrayToInputFieldStore(orderLineList: any[]) {
    return orderLineList.map((el) => convertInputFieldStore(el))
}

export function convertInputFieldStore(el) {
    const acceptedUser = new FieldStore<{ id: string }>([ required(() => 'Please, enter an user') ])
    const meal = new FieldStore<{ id: string }>([ required(() => 'Please, select a meal') ])
    const quantity = new FieldStore<number>([ required(() => 'Please, enter a quantity'), positiveNumeric ])
    const status = new FieldStore<string>([ required(() => 'Please, select a status') ])
    const unitPrice = new FieldStore<number>([ required(() => 'Please, enter a unit price'), positiveNumeric ])

    acceptedUser.set({
        id: el.acceptedUser && el.acceptedUser.id ? el.acceptedUser.id: null
    });
    meal.set({
        id: el.meal && el.meal.id ? el.meal.id : null
    });
    quantity.set(el.quantity || 0);
    status.set(el.status || 'PREPARE');
    unitPrice.set(el.unitPrice || 0);
    return {
        acceptedUser,
        meal,
        quantity,
        status,
        unitPrice,
        id: el.id
    }
}
