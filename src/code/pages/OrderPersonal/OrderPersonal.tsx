import React from "react";
import { observer } from "mobx-react";
import { OrderPersonalStore } from "./OrderPersonalStore";
import {Button, InputItem, Label, Option, Select} from "~/code/components";
import styles from './OrderPersonal.scss'
import {Card, Collapse, Icon, Modal} from "antd";
import {toJS} from "mobx";
const { Panel } = Collapse;

export default observer(({ store }: { store: OrderPersonalStore }) => {
    const genExtra = (index) => (
        <Icon type="close" onClick={event => {
            store.closeMeal(index)
            event.stopPropagation();
        }} />
    );
    return (
        <div className={ styles.OrderPersonal }>
            <div className={ styles.filterRow }>
                <div>
                    <Label>Waiter</Label>
                </div>

                <Select
                    style={{
                        width: '250px'
                    }}
                    defaultValue={ store.acceptedUser.value }
                    value={ store.acceptedUser.value }
                    disabled={ store.isSending || store.isLoading }
                    loading={ store.isLoading }
                    onChange={ (v) => store.onUserChange(v) }
                    store={ store.acceptedUser }
                >
                    {
                        store.users.map(({ label, value  }) => (
                            <Option key={ value } value={ value }>{ label }</Option>
                        ))
                    }
                </Select>
            </div>
            <div className={ styles.filterRow }>
                <div>
                    <Label>Table</Label>
                </div>

                <Select
                    style={{
                        width: '250px'
                    }}
                    defaultValue={ store.table.value }
                    value={ store.table.value }
                    disabled={ store.isSending || store.isLoading }
                    loading={ store.isLoading }
                    onChange={ (v) => store.onTableChange(v) }
                    store={ store.table }
                >
                    {
                        store.tables.map(({ label, value  }) => (
                            <Option key={ value } value={ value }>{ label }</Option>
                        ))
                    }
                </Select>
            </div>

            <div className={ `${ styles.filterRow }` }>
                <div className={`${ styles.mealsHeader }`}>
                    <Label>Meal</Label>
                    <Button onClick={ () => store.addNewMeal() }> Add a new meal</Button>
                </div>
                <Collapse bordered={false}>
                {
                    store.orderLineList && store.orderLineList && store.orderLineList.map((el, index) => {
                        return (
                            <Panel
                                header={ `Meal #${ index }` }
                                key={ index }
                                extra={genExtra(index)}
                            >
                                <div>
                                    <div>
                                        <Label>Waiter</Label>
                                    </div>

                                    <Select
                                        style={{
                                            width: '250px'
                                        }}
                                        defaultValue={ el.acceptedUser.value.id }
                                        value={ el.acceptedUser.value.id }
                                        disabled={ store.isSending || store.isLoading }
                                        loading={ store.isLoading }
                                        onChange={ (v) => store.onOrderLineChange({
                                            id: v
                                        }, 'acceptedUser', index) }
                                    >
                                        {
                                            store.users.map(({ label, value  }) => (
                                                <Option key={ value } value={ value }>{ label }</Option>
                                            ))
                                        }
                                    </Select>
                                </div>
                                <div>
                                    <div>
                                        <Label>Meal</Label>
                                    </div>

                                    <Select
                                        style={{
                                            width: '250px'
                                        }}
                                        defaultValue={ el.meal.value.id }
                                        value={ el.meal.value.id }
                                        disabled={ store.isSending || store.isLoading }
                                        loading={ store.isLoading }
                                        showSearch={ true }
                                        optionFilterProp="children"
                                        onChange={ (v) => store.onOrderLineMealChange(v, index) }
                                    >
                                        {
                                            store.meals.map(({ label, value  }) => (
                                                <Option key={ value } value={ value }>{ label }</Option>
                                            ))
                                        }
                                    </Select>
                                </div>
                                <div>
                                    <InputItem
                                        label={ 'Quantity' }
                                        store={ el.quantity }
                                        placeholder={ 'Quantity' }
                                        inputProps={{
                                            name: 'Quantity',
                                            disabled: store.isSending
                                        }}
                                        isNumeric={ true }
                                    />
                                </div>
                                <div>
                                    <div>
                                        <Label>Status</Label>
                                    </div>

                                    <Select
                                        style={{
                                            width: '250px'
                                        }}
                                        defaultValue={ el.status.value }
                                        value={ el.status.value }
                                        disabled={ store.isSending || store.isLoading }
                                        loading={ store.isLoading }
                                        showSearch={ true }
                                        optionFilterProp="children"
                                        onChange={ (v) => store.onOrderLineChange(v, 'status', index) }
                                    >
                                        {
                                            store.statuses.map(({ label, value  }) => (
                                                <Option key={ value } value={ value }>{ label }</Option>
                                            ))
                                        }
                                    </Select>
                                </div>
                                <div>
                                    <InputItem
                                        label={ 'Unit price' }
                                        store={ el.unitPrice }
                                        placeholder={ 'Unit price' }
                                        isNumeric={ true }
                                        inputProps={{
                                            name: 'Unit price',
                                            disabled: store.isSending
                                        }}
                                    />
                                </div>
                            </Panel>
                        )
                    })
                }
                </Collapse>
            </div>

            <div className={ styles.filterRow }>
                <Button key="back" href={'/orders'}>
                    Back
                </Button>
                <Button
                    key="submit"
                    type="primary"
                    loading={ store.isSending }
                    onClick={ () => store.save() }
                    style={{
                        marginLeft: '10px'
                    }}
                >
                    Save
                </Button>
            </div>
        </div>
    )
})
