import {apiCheck, noThrow} from "back-connector";
import {getWithToken} from "~/code/services/fetchers";
import moment from "moment";
import {StaticDateType} from "~/code/pages/Dashboard/models/StaticDateType";

export async function loadIncomeGraphs(type) {
    return noThrow<any>(apiCheck(getWithToken('/api/incomes/statistic', getDateByType(type))))
}
export async function loadPopularMealsGraphs(type) {
    return noThrow<any>(apiCheck(getWithToken('/api/meals/statistic', getDateByType(type))))
}


function getDateByType(type) {
    switch (type) {
        case StaticDateType.MONTHLY:
            return {
                dateGte: moment().subtract(1, 'months').format("YYYY-MM-DD"),
                dateLte: moment().format("YYYY-MM-DD"),
                type: 'DAILY'
            }
        case StaticDateType.YEARLY:
            return {
                dateGte: moment().subtract(12, 'months').format("YYYY-MM-DD"),
                dateLte: moment().format("YYYY-MM-DD"),
                type: 'DAILY'
            }
        default:
            return null

    }
}
