export enum StaticDateType {
    MONTHLY = 'MONTHLY',
    WEEKLY = 'WEEKLY',
    DAILY = 'DAILY',
    YEARLY = 'YEARLY'
}
