import {observable, reaction} from "mobx";
import {message} from "antd";
import {StaticDateType} from "~/code/pages/Dashboard/models/StaticDateType";
import {loadIncomeGraphs, loadPopularMealsGraphs} from "~/code/pages/Dashboard/services/fetchers";
import {compare} from "~/code/pages/Dashboard/services/sorters";

export class DashboardStore {
    private parentStore;

    constructor(parentStore) {
        this.parentStore = parentStore
        reaction(() => this.incomeMode, async (v) => {
            this.incomeGraphValues = [];
            const result = await loadIncomeGraphs(v);
            if(result.error) {
                message.error('Request failed! Can\'t load income ')
            }

            this.incomeGraphValues = result.value.values
        }, {
            fireImmediately: true
        })
        reaction(() => this.popularMealMode, async (v) => {
            this.popularMealsGraphValues = [];
            const result = await loadPopularMealsGraphs(v);
            if(result.error) {
                message.error('Request failed! Can\'t load popular meals ')
            }

            this.popularMealsGraphValues = result.value.values.sort(compare);
        }, {
            fireImmediately: true
        })
    }

    @observable
    public incomeMode: StaticDateType = StaticDateType.MONTHLY

    @observable
    public popularMealMode: StaticDateType = StaticDateType.MONTHLY

    @observable
    public incomeGraphValues: any[] =  [];

    @observable
    public popularMealsGraphValues: any[] =  [
        {
            name: 'Page A', value: 5000
        },
        {
            name: 'Page B', value: 3000
        },
        {
            name: 'Page C', value: 7000
        }
    ];

    @observable
    public handleModeChange(name, e) {
        this[name] = e.target.value
    }
}
