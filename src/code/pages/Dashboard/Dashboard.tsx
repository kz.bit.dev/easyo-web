import React from "react";
import {observer} from "mobx-react";
import { Tabs, Radio } from 'antd';
import {
    AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer, BarChart, Legend, Bar
} from 'recharts';
import {DashboardStore} from "./DashboardStore";
import styles from './Dashboard.scss'
import {StaticDateType} from "~/code/pages/Dashboard/models/StaticDateType";

export default observer( ({ store }: { store: DashboardStore}) => {

    return (
        <div className={ styles.Dashboard }>
            <div style={{ width: '100%' }}>
                <div className={ styles.header }>
                    <div>
                        Income
                    </div>
                    <div>
                        <Radio.Group onChange={ (v) => store.handleModeChange('incomeMode', v) } value={ store.incomeMode } style={{ marginBottom: 8 }}>
                            <Radio.Button value={ StaticDateType.MONTHLY }> { StaticDateType.MONTHLY  }</Radio.Button>
                            <Radio.Button value={ StaticDateType.YEARLY }>{ StaticDateType.YEARLY }</Radio.Button>
                        </Radio.Group>
                    </div>
                </div>
                <div style={{ width: '100%', height: 300 }}>
                    <ResponsiveContainer>
                        <AreaChart data={ store.incomeGraphValues } margin={{top: 10, right: 30, left: 0, bottom: 0}}>
                            <CartesianGrid strokeDasharray="3 3"/>
                            <XAxis dataKey="name"/>
                            <YAxis/>
                            <Tooltip/>
                            <Area type='monotone' dataKey='value' stroke='#acd5f2' fill='#E8F3FB' />
                        </AreaChart>
                    </ResponsiveContainer>
                </div>
            </div>

            <div style={{ width: '100%' }}>
                <div className={ styles.header }>
                    <div>
                        Popular meals
                    </div>
                    <div>
                        <Radio.Group onChange={ (v) => store.handleModeChange('popularMealMode', v) } value={ store.popularMealMode } style={{ marginBottom: 8 }}>
                            <Radio.Button value={ StaticDateType.MONTHLY }> { StaticDateType.MONTHLY  }</Radio.Button>
                            <Radio.Button value={ StaticDateType.YEARLY }>{ StaticDateType.YEARLY }</Radio.Button>
                        </Radio.Group>
                    </div>
                </div>
                <div style={{ width: '100%', height: 300 }}>
                    <ResponsiveContainer>
                        <BarChart
                            data={ store.popularMealsGraphValues }
                            margin={{
                                top: 5, right: 30, left: 20, bottom: 5
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="name" />
                            <YAxis />
                            <Tooltip />
                            <Legend />
                            <Bar dataKey="value" fill="#E8F3FB" />
                        </BarChart>
                    </ResponsiveContainer>
                </div>

            </div>
        </div>
    )
})
