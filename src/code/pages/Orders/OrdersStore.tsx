import React from "react";
import {action, observable, reaction} from "mobx";
import { Button, message } from "antd";
import {fetchAllOrders, fetchDeleteOrder, getOrderColumns} from "~/code/pages/Orders/services";
import styles from './Orders.scss'
import {fetchAllUsers} from "~/code/pages/Users/services";
import {fetchAllTables} from "~/code/pages/Tables/services";

export class OrdersStore {
    private parentStore;

    constructor(parentStore) {
        this.parentStore = parentStore
        this.init()
        reaction(() => ({
            currentPage: this.currentPage,
            filter: this.filter
        }), () => {
            this.loadOrders()
        })
    }

    @observable
    public loading: boolean = false;

    @observable
    public orders: any[] = [];

    @observable
    public users: any[] = [];

    @observable
    public tables: any[] = [];

    @observable
    public total: number = 1;

    @observable
    public currentPage: number = 0;

    @observable
    public filter: any = {};

    @action
    init() {
        this.loadUsers()
        this.loadOrders()
        this.loadTables()
    }

    @action
    onPageChange(value) {
        this.currentPage = value - 1
    }

    @action
    onFilterChange(name, value) {
        this.filter = {
            ...this.filter,
            [name]: value
        }
    }

    @action
    async loadUsers() {
        this.loading = true;
        const result = await fetchAllUsers({ currentPage: 0 });
        this.loading = false;
        if(result.error) {
            message.error('Request failed! Can\'t load users ')
        }

        this.users = result.value.data.map((el) => ({
            label: (el.firstName ? el.firstName : '') + ' ' +  (el.lastName ?  el.lastName : ''),
            value: el.id
        }));
    }

    @action
    async loadTables() {
        this.loading = true;
        const result = await fetchAllTables({ currentPage: 0 });
        this.loading = false;
        if(result.error) {
            message.error('Request failed! Can\'t load tables ')
        }

        this.tables = result.value.data.map((el) => ({
            label: el.number,
            value: el.id
        }));
    }

    @action
    async loadOrders() {
        this.loading = true;
        const result = await fetchAllOrders({ currentPage: this.currentPage, size: 10, filter: this.filter });
        this.loading = false;
        if(result.error) {
            message.error('Request failed! Can\'t load orders ')
        }

        this.orders = result.value.data;
        this.total = result.value.total;

        if(this.orders.length === 0 && this.currentPage !== 0) {
            this.currentPage = 0
        }

    }

    @action
    refreshPage() {
        this.loadOrders()
    }

    @action
    async onDeleteOrder(order) {
        const result = await fetchDeleteOrder(order.id)
        if(!result.error) {
            message.success(' Order deleted');
            this.refreshPage();
            return;
        }
        message.error(result.error.message);
    }

    getGridColumns() {
        const columns = getOrderColumns(this.users, this.tables);
        columns.push(
            {
                title: 'Actions',
                field: 'actions',
                render: (text, order) => {
                    return (
                        <>
                            <Button href={ `/orders/${ order.id }`}>
                                EDIT
                            </Button>
                            <Button onClick={ () => this.onDeleteOrder(order) } className={ styles.deleteButton }>
                                DELETE
                            </Button>
                        </>
                    )
                }
            }
        )
        return columns;
    }
}
