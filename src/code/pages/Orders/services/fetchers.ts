import {apiCheck, noThrow} from "back-connector";
import {delWithToken, getWithToken} from "~/code/services/fetchers";

export async function fetchAllOrders({ currentPage, size, filter }: { currentPage: number, size: number, filter?: any }) {
    return noThrow<any>(apiCheck(getWithToken('/api/orders',  { page: currentPage, size, ...(filter ? filter : {}) })))// Todo: change
}

export async function fetchDeleteOrder(id: string) {
    return noThrow(apiCheck(delWithToken('/api/orders/' + id)))
}
