import React from "react";
import { OrderColumn } from "~/code/pages/Orders/models/OrderColumn";
import moment from "moment";

export function getOrderColumns(users, tables): OrderColumn [] {
    return [
        {
            title: 'ID',
            field: 'id'
        },{
            title: 'Ordered Date',
            render: (text, record) => {
                return moment(record.orderedDate).format("YYYY-MM-DD HH:mm:ss")
            }
        },
        {
            title: 'Waiter',
            render: (text, record) => {
                const item = users.find(el => el.value === record.acceptedUser.id)
                return item && item.label
            }
        },
        {
            title: 'Table',
            render: (text, record) => {
                const item = tables.find(el => el.value === record.table.id)
                return item && item.label
            }
        }
    ]
}
