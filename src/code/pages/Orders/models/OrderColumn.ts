export interface OrderColumn {
    title: string,
    field?: string,
    render?: (text: any, record: any) => any
}
