import React from 'react';
import { observer } from "mobx-react";
import { Select, DatePicker } from "antd";
const { RangePicker } = DatePicker;
import { Button, Grid } from "~/code/components";
import { OrdersStore } from "./OrdersStore";
import { OrderColumn } from "./models";
import { Column}  from "~/code/components/Grid";
const { Option } = Select;
import styles from './Orders.scss'

export default observer(({ store }: { store: OrdersStore }) => {
    return (
        <div className={ styles.Orders }>
            <div>
                <div className={ styles.filterRow }>
                    <h4>
                        Waiter
                    </h4>
                    <Select
                        style={{
                            width: '200px'
                        }}
                        value={ store.filter ? store.filter.userId : '' }
                        showSearch={ true }
                        optionFilterProp="children"
                        onChange={ (v) => store.onFilterChange('userId', v) }
                    >
                        {
                            store.users.map(({ label, value  }) => (
                                <Option key={ value } value={ value }>{ label }</Option>
                            ))
                        }
                    </Select>
                </div>
                <div className={ styles.filterRow }>
                    <h4>
                        Table
                    </h4>
                    <Select
                        style={{
                            width: '200px'
                        }}
                        value={ store.filter ? store.filter.tableId : '' }
                        showSearch={ true }
                        optionFilterProp="children"
                        onChange={ (v) => store.onFilterChange('tableId', v) }
                    >
                        {
                            store.tables.map(({ label, value  }) => (
                                <Option key={ value } value={ value }>{ label }</Option>
                            ))
                        }
                    </Select>
                </div>
                <div className={ styles.filterRow }>
                    <h4>
                        Order date
                    </h4>
                    <RangePicker onChange={ (v) => {
                        store.onFilterChange('orderDateGte', v && v[0] ? v[0].format("YYYY-MM-DD"): null);
                        store.onFilterChange('orderDateLte', v && v[1] ? v[1].format("YYYY-MM-DD"): null);
                    }} />
                </div>
            </div>

            <div className={ styles.createOrderButton }>
                <Button href={ '/orders/create' }> Create order </Button>
            </div>
            <Grid<OrderColumn>
                columns={ store.getGridColumns() as Column<OrderColumn>[]}
                data={ store.orders }
                loading={ store.loading }
                rowKey={ 'id' as keyof OrderColumn }
                pagination={{
                    position: 'both',
                    total: store.total,
                    defaultPageSize: 10,
                    current: store.currentPage + 1,
                    onChange: (v) => store.onPageChange(v)
                }}
                scroll={ { x: true } }
            />
        </div>
    )
})
