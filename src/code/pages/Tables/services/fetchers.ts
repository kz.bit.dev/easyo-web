import {apiCheck, noThrow} from "back-connector";
import {delWithToken, getWithToken} from "~/code/services/fetchers";

export async function fetchAllTables({ currentPage, size }: { currentPage: number, size?: number }) {
    return noThrow<any>(apiCheck(getWithToken('/api/tables',  { page: currentPage, ...(size ? { size } : {}) })))// Todo: change
}

export async function fetchDeleteTable(id: string) {
    return noThrow(apiCheck(delWithToken('/api/tables/' + id)))
}
