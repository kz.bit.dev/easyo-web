import React from "react";
import { TableColumn } from "~/code/pages/Tables/models/TableColumn";
import { Button } from "~/code/components";
import styles from './../Tables.scss'

export function getTableColumns(): TableColumn [] {
    return [
        {
            title: 'ID',
            field: 'id'
        },
        {
            title: 'Number',
            field: 'number'
        },
        {
            title: 'Size',
            field: 'size'
        },
        {
            title: 'Status',
            field: '',
            render: (item: { reserved: boolean }) => {
                if (item.reserved) {
                    return (
                        <Button className={ styles.reservedButton }> Reserved </Button>
                    )
                }
                return (
                    <Button className={ styles.freeButton }> Free </Button>
                )
            }
        }

    ]
}
