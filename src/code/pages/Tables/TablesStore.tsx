import React from "react";
import {action, observable, reaction} from "mobx";
import { Button, message } from "antd";
import {fetchAllTables, fetchDeleteTable, getTableColumns} from "~/code/pages/Tables/services";
import { TablePersonalModalStore } from "./components/TablePersonalModal/TablePersonalModalStore";
import styles from './Tables.scss'

export class TablesStore {
    public tablePersonalModalStore;
    private parentStore;

    constructor(parentStore) {
        this.parentStore = parentStore
        this.tablePersonalModalStore = new TablePersonalModalStore(this);
        this.init()
        reaction(() => this.currentPage, () => {
            this.loadTables()
        })
    }

    @observable
    public loading: boolean = false;

    @observable
    public tables: any[] = [];

    @observable
    public total: number = 1;

    @observable
    public currentPage: number = 0;

    @action
    init() {
        this.loadTables()
    }

    @action
    onPageChange(value) {
        this.currentPage = value - 1
    }

    @action
    async loadTables() {
        this.loading = true;
        const result = await fetchAllTables({ currentPage: this.currentPage, size: 10 });
        this.loading = false;
        if(result.error) {
            message.error('Request failed! Can\'t load tables ')
        }

        this.tables = result.value.data;
        this.total = result.value.total;

        if(this.tables.length === 0 && this.currentPage !== 0) {
            this.currentPage = 0
        }

    }

    @action
    refreshPage() {
        this.loadTables()
    }

    @action
    createTable() {
        this.tablePersonalModalStore.showModal();
    }

    @action
    onEditTable(table) {
        this.tablePersonalModalStore.showModal();
        this.tablePersonalModalStore.setTable(table)
    }

    @action
    async onDeleteTable(table) {
        const result = await fetchDeleteTable(table.id)
        if(!result.error) {
            message.success(' Table deleted');
            this.refreshPage();
            return;
        }
        message.error(result.error.message);
    }

    getGridColumns() {
        const columns = getTableColumns();
        columns.push(
            {
                title: 'Actions',
                field: 'actions',
                render: (text, table) => {
                    return (
                        <>
                            <Button onClick={ () => this.onEditTable(table) }>
                                EDIT
                            </Button>
                            <Button onClick={ () => this.onDeleteTable(table) } className={ styles.deleteButton }>
                                DELETE
                            </Button>
                        </>
                    )
                }
            }
        )
        return columns;
    }
}
