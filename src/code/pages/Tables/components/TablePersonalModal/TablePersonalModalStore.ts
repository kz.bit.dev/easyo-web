import {
    action, computed,
    observable,
    runInAction
} from "mobx";
import { message } from "antd";
import FieldStore from "~/code/services/field-store";
import { required } from "~/code/services/validators";
import { isValidArr } from "~/code/services/field-store/services";
import { createTable, editTable } from "./services";

export class TablePersonalModalStore {
    private parentStore;
    constructor(parentStore) {
        this.parentStore = parentStore;
    }
    public tableNumber = new FieldStore<string>([ required(() => 'Please, enter number of table') ]);
    public size = new FieldStore<string>([ required(() => 'Please, enter size of table') ]);
    public reserved = new FieldStore<boolean>([]);

    @observable
    public isVisible: boolean = false;

    @observable
    public id: string = '';

    @observable
    public isSending: boolean = false;

    @action
    showModal() {
        this.isVisible = true
    }

    @computed
    public get isEditMode() {
        return !!this.id
    }

    @action
    closeModal() {
        this.isVisible = false;
        this.tableNumber.set('');
        this.size.set('');
        this.reserved.set(false);
        this.id = null;
    }

    @action
    onReserveChange(e) {
        this.reserved.set(e.target.value);
    }

    @action
    async save() {
        if (!isValidArr(this.tableNumber, this.size)) return;

        const table = {
            number: this.tableNumber.value,
            size: this.size.value,
            reserved: this.reserved.value
        };

        this.isSending = true;

        const result = this.isEditMode ? await editTable(this.id, table) : await createTable(table);

        if (!result.error) {
            this.isSending = false;
            message.success(this.isEditMode ? 'Table edited' : 'Table created');
            this.closeModal();
            this.parentStore.refreshPage();
            return;
        }

        runInAction(() => {
            if (!result.error) return;
            this.isSending = false;
            message.error(result.error.message)
        })
    }

    @action
    setTable(table) {
        this.id = table.id;
        this.tableNumber.set(table.number);
        this.size.set(table.size);
        this.reserved.set(table.reserved);
    }
}
