import React from "react";
import { observer } from "mobx-react";
import { Modal } from "antd";
import { TablePersonalModalStore } from "./TablePersonalModalStore";
import { Button, InputItem, Radio } from "~/code/components";
import styles from './TablePersonalModal.scss'

export default observer(({ store }: { store: TablePersonalModalStore }) => {
    return (
        <div className={ styles.TablePersonalModal }>
            <Modal
                title={ store.id ? `Table #${ store.id }` : 'Create a new table' }
                onCancel={ () => store.closeModal() }
                visible={ store.isVisible }
                onOk={ () => store.save() }
                okText={ 'Save' }
                footer={[
                    <Button key="back" onClick={ () => store.closeModal() }>
                        Back
                    </Button>,
                    <Button key="submit" type="primary" loading={ store.isSending } onClick={ () => store.save() }>
                        Save
                    </Button>
                ]}
            >
                <InputItem
                    isNumeric={ true }
                    label={ 'Number of table' }
                    store={ store.tableNumber }
                    placeholder={ '' }
                    inputProps={{
                        type: 'number',
                        name: 'number_table_field',
                        disabled: store.isSending
                    }}
                />
                <InputItem
                    isNumeric={ true }
                    label={ 'Size of table' }
                    store={ store.size }
                    placeholder={ '' }
                    inputProps={{
                        type: 'number',
                        name: 'size_field',
                        disabled: store.isSending
                    }}
                />

                <Radio.Group defaultValue={ store.reserved.value } onChange={ (v) => store.onReserveChange(v) }>
                    <Radio.Button value={ false }>Free</Radio.Button>
                    <Radio.Button value={ true }>Reserved</Radio.Button>
                </Radio.Group>
            </Modal>
        </div>
    )
})
