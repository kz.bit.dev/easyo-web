export interface Table {
    id?: string,
    number: string
    size: string,
    reserved: boolean
}
