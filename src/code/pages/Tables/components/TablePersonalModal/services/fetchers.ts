import { Table } from "././../models";
import { postWithToken, putWithToken } from "~/code/services/fetchers";
import {apiCheck, noThrow, post} from "back-connector";
import uuid from 'uuid/v4'

export function createTable(table: Table) {
    return noThrow(apiCheck(postWithToken('/api/tables', table)))
}

export function editTable(id: string, table: Table) {
    return noThrow(apiCheck(putWithToken('/api/tables/' + id, table)))
}
