export interface TableColumn {
    title: string,
    field: string,
    render?: (text: any, record: any) => any
}
