import React from 'react';
import { observer } from "mobx-react";
import {Button, Grid} from "~/code/components";
import { TablesStore } from "./TablesStore";
import { TableColumn } from "./models";
import { Column}  from "~/code/components/Grid";
import { TablePersonalModal } from "./components";
import styles from './Tables.scss'

export default observer(({ store }: { store: TablesStore }) => {
    return (
        <div className={ styles.Tables }>
            <div className={ styles.createTableButton }>
                <Button onClick={ () => store.createTable() }> Create table </Button>
            </div>
            <Grid<TableColumn>
                columns={ store.getGridColumns() as Column<TableColumn>[]}
                data={ store.tables }
                loading={ store.loading }
                rowKey={ 'id' as keyof TableColumn }
                pagination={{
                    position: 'both',
                    total: store.total,
                    defaultPageSize: 10,
                    current: store.currentPage + 1,
                    onChange: (v) => store.onPageChange(v)
                }}
                scroll={ { x: true } }
            />
            { store.tablePersonalModalStore.isVisible && (
                <TablePersonalModal store={ store.tablePersonalModalStore } />
            ) }

        </div>
    )
})
