import { LoginUser } from "./LoginUser";

export interface LoginResponse {
    "id": null,
    "token": string,
    "user": LoginUser
}
