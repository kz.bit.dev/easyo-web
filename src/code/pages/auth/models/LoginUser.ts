export interface LoginUser {
    "id": number,
    "deleted": number | boolean,
    "imageUrl": string,
    "firstName": string,
    "lastName": string,
    "email": string
}
