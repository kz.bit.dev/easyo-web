import {action, observable, runInAction} from 'mobx'
import FieldStore from '~/code/services/field-store'
import { isValidArr } from "~/code/services/field-store/services";
import { required, email } from "~/code/services/validators";
import { AuthStore } from "~/code/pages/auth/AuthStore";
import { AppStore } from "~/code/AppStore";

export default class LoginStore {

    public email = new FieldStore<string>([ required(() => 'Please, enter your email'), email ]);

    public password = new FieldStore<string>([ required(() => 'Please, enter your password')])

    @observable
    public isPasswordShown = false;

    @observable
    public isSending = false;

    @observable
    public error: string | JSX.Element;

    @action
    public async login() {
        if (!isValidArr(this.email, this.password)) return

        this.isSending = true;

        const result = await AuthStore.login({
            login: this.email.value,
            password: this.password.value
        });

        if (!result.error) {
            this.isSending = false;
            await AppStore.onLogin()
        }

        runInAction(() => {
            if (!result.error) return;
            this.isSending = false;
            this.error = result.error.message
        })
    }

    @action
    public togglePasswordVisibility() {
        this.isPasswordShown = !this.isPasswordShown
    }
}
