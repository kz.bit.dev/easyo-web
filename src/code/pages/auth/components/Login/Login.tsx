import React, { FormEvent } from 'react'
import { observer } from 'mobx-react'
import { Icon } from "antd";
import {
    Button,
    Form,
    FormItem,
    InputItem
} from '~/code/components'
import LoginStore from './LoginStore'
import translations from './translations'
import { Helmet } from 'react-helmet'
import styles from './Login.scss'

export default observer(({ store }: { store: LoginStore }) => {

    function submit(e: FormEvent) {
        store.login()
        e.preventDefault()
    }

    const { isPasswordShown } = store

    return (
        <div className={ styles.Login }>
            <div data-semantic-id="login_form">
                <div className={ styles.header }>
                    <span data-semantic-id="login_title" className={styles.title}> { translations().loginTitle } </span>
                </div>
                <Helmet>
                    <title>
                        Login
                    </title>
                </Helmet>
                <div className={ styles.main }>
                    <Form onSubmit={submit}>
                        <InputItem
                            store={store.email}
                            placeholder={translations().email}
                            inputProps={{
                                name: 'email_field',
                                size: 'large',
                                disabled: store.isSending
                            }}
                        />
                        <InputItem
                            store={ store.password }
                            type={ isPasswordShown ? 'text' : 'password' }
                            placeholder={ translations().password }
                            inputProps={{
                                    name: 'password_field',
                                    disabled: store.isSending,
                                    suffix: (
                                       <span
                                           data-semantic-id="show_password_button"
                                           className={ `${styles.showPasswordButton} ${isPasswordShown ? styles.active : ''}`}
                                           onClick={() => store.togglePasswordVisibility()}>
                                           {
                                               isPasswordShown ? <Icon type="eye-invisible-fill" theme="filled" /> : <Icon type="eye-fill" theme="filled" />
                                           }
                                        </span>
                                   ),
                                   size: 'large'
                            }} />
                        { store.error &&
                            <div className={ styles.error }>
                                {store.error}
                            </div>
                        }
                        <FormItem>
                            <Button
                                data-semantic-id="login_button"
                                disabled={ store.isSending }
                                type="primary"
                                htmlType="submit"
                                size="large"
                                className={styles.mainButton}
                            >
                                Login
                            </Button>
                        </FormItem>
                    </Form>
                </div>
            </div>
        </div>
    )
})
