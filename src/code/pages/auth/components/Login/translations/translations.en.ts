import translations from './translations'

translations.add('en', {
    email: 'Email',
    password: 'Password',
    loginTitle: 'Login'
})
