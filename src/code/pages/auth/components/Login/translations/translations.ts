import { TranslationBuilder } from '~/code/components/Translation'

export default TranslationBuilder.create<{
    email: string,
    password: string,
    loginTitle: string
}>()

