import {
    get,
    setWithMutex
} from "store-connector";
import config from "~/code/config";
import { LoginUser } from "~/code/pages/auth/models";

export function saveUserToken(token: string) {
    return setWithMutex(config.auth.TOKEN, token, { useUserHash: false })
}

export function getUserToken() {
    return get<string>(config.auth.TOKEN, { useUserHash: false })
}

export function saveUserInfo(userInfo: LoginUser) {
    return setWithMutex(config.auth.USER_INFO, userInfo, { useUserHash: false })
}

export function getUserInfo() {
    const userInfo = get<LoginUser>(config.auth.USER_INFO, { useUserHash: false })
    return userInfo ? userInfo : { userLogin: 'anonymous', userHash: 0 }
}

export function isExistActiveInstance(currentThreadId: string): Promise<boolean> {
    const threadId = currentThreadId
    let eventHandler: (event: StorageEvent) => void
    return Promise.race<boolean>([
        new Promise<boolean>((resolve, reject) => {
            let eventRecieved = false
            eventHandler = (event: StorageEvent) => {
                if (event.key === 'instanceWatcher') {
                    eventRecieved = event.newValue !== threadId
                    if (eventRecieved) {
                        window.removeEventListener('storage', eventHandler)
                        resolve(true)
                    }
                }
            }
            window.addEventListener('storage', eventHandler)
            localStorage.setItem('instanceCaller', threadId)
        })
        ,
        new Promise<boolean>((resolve, reject) => {
            setTimeout(() => {
                window.removeEventListener('storage', eventHandler)
                resolve(false)
            }, 1000)
        })]
    )
}
