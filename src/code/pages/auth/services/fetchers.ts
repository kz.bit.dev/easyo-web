import {
    LoginRequest,
    LoginResponse
} from "~/code/pages/auth/models";
import { post } from "back-connector";

export const login = (request: LoginRequest) => post<LoginResponse>(
    '/api/sessions/auth',
    {
        login: request.login,
        password: request.password
    }
);
