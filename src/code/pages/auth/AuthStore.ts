import { action, observable, runInAction } from 'mobx';
import { apiCheck, FlowResult, noThrow } from 'back-connector';
import { LoginRequest } from '~/code/pages/auth/models';
import {getUserToken, isExistActiveInstance, login, saveUserInfo, saveUserToken} from '~/code/pages/auth/services';
import { setWithMutex } from 'store-connector';
import config from '~/code/config';
import isEmpty from 'lodash/isEmpty';
import uuid from 'uuid/v4'

class AuthStore {

    constructor() {
        this.instanceDetectionResponder = this.instanceDetectionResponder.bind(this)
        this.initialize()
    }

    private tokenPromise: Promise<FlowResult<void>>;
    @observable
    public initialized: boolean = false;

    @observable
    public isAuthenticated: boolean = false;

    @observable
    public ThreadId: string = uuid();


    @action
    private initialize() {
        let isWindowRefreshed = false;
        if (isEmpty(window.name)) {
            window.name = this.ThreadId
        } else {
            isWindowRefreshed = true;
            this.ThreadId = window.name
        }
        window.removeEventListener('storage', this.instanceDetectionResponder);
        window.addEventListener('storage', this.instanceDetectionResponder);
        const isValid = !!getUserToken();
        if (isValid && !isWindowRefreshed) {
            noThrow(async () => {
                const isInstanceActive = await isExistActiveInstance(this.ThreadId)
                if (isInstanceActive) {
                    runInAction(() => {
                        this.isAuthenticated = true
                        this.initialized = true
                    })
                } else {
                    this.logout()
                    runInAction(() => {
                        this.isAuthenticated = false
                        this.initialized = true
                    })
                }
            })
        } else {
            if (isValid && isWindowRefreshed) {
                this.isAuthenticated = true
            } else {
                this.isAuthenticated = false
            }
            this.initialized = true
        }
    }

    public instanceDetectionResponder(event: StorageEvent) {
        if (event.key === 'instanceCaller' && event.newValue !== this.ThreadId) {
            localStorage.setItem('instanceWatcher', this.ThreadId)
        }
    }

    public async login(request: LoginRequest) {

        const promise = noThrow(async () => {
            const response = await apiCheck(login(request));
            await saveUserInfo(response.user);
            await saveUserToken(response.token);
            this.initialize()
        });

        this.tokenPromise = promise;

        const result = await promise;

        if (this.tokenPromise === promise) {
            this.tokenPromise = undefined
        }

        return result
    }

    public logout() { // TODO: change
        noThrow(setWithMutex(config.auth.TOKEN, undefined, { useUserHash: false }))
        noThrow(setWithMutex(config.auth.USER_INFO, undefined, { useUserHash: false }))
        this.isAuthenticated = false;
    }
}
const AuthStoreInstance = new AuthStore();
export { AuthStoreInstance as AuthStore }
