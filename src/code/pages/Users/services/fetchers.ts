import {apiCheck, noThrow} from "back-connector";
import {delWithToken, getWithToken} from "~/code/services/fetchers";

export async function fetchAllUsers({ currentPage, size }: { currentPage: number, size?: number }) {
    return noThrow<any>(apiCheck(getWithToken('/api/users',  { page: currentPage, ...(size ? { size } : {}) })))// Todo: change
}

export async function fetchDeleteUser(id: string) {
    return noThrow(apiCheck(delWithToken('/api/users/' + id)))
}
