import React from "react";
import { UserColumn } from "~/code/pages/Users/models/UserColumn";

export function getUserColumns(): UserColumn [] {
    return [
        {
            title: 'ID',
            field: 'id'
        },
        {
            title: 'FirstName',
            field: 'firstName'
        },
        {
            title: 'LastName',
            field: 'lastName'
        },
        {
            title: 'Email',
            field: 'email'
        },
        {
            title: 'Position',
            field: 'userPositionEnum'
        }
    ]
}
