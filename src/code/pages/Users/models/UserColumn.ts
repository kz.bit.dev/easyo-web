export interface UserColumn {
    title: string,
    field: string,
    render?: (text: any, record: any) => any
}
