import React from "react";
import { observer } from "mobx-react";
import { Icon, Modal, Upload } from "antd";
import { UserPersonalModalStore } from "./UserPersonalModalStore";
import { Button, InputItem, Select, Option } from "~/code/components";
import { getPositionsList } from "./services";
import styles from './UserPersonalModal.scss'
import Label from "~/code/components/Label/Label";
import { uploadFile } from "~/code/services/fetchers";

export default observer(({ store }: { store: UserPersonalModalStore }) => {
    return (
        <div className={ styles.UserPersonalModal }>
            <Modal
                title={ store.id ? `User #${ store.id }` : 'Create a new user' }
                onCancel={ () => store.closeModal() }
                visible={ store.isVisible }
                onOk={ () => store.save() }
                okText={ 'Save' }
                footer={[
                    <Button key="back" onClick={ () => store.closeModal() }>
                        Back
                    </Button>,
                    <Button key="submit" type="primary" loading={ store.isSending } onClick={ () => store.save() }>
                        Save
                    </Button>
                ]}
            >
                <Label>Profile image</Label>
                <Upload
                    listType="picture-card"
                    fileList={ store.fileList }
                    onRemove={ () => store.onImageLoaded(null) }
                    customRequest={ async ({ onSuccess, onError, file }) => {
                        const result = await uploadFile(file);
                        if(!result.error) {
                            store.onImageLoaded(result.value);
                            return onSuccess(null, file);
                        }
                        onError(result.error)
                    }}
                >
                    <div>
                        <Icon type={ 'plus'} />
                        <div className="ant-upload-text">Upload</div>
                    </div>
                </Upload>
                <InputItem
                    label={ 'FirstName' }
                    store={ store.firstName }
                    placeholder={ 'FirstName' }
                    inputProps={{
                        name: 'firstname_field',
                        disabled: store.isSending
                    }}
                />
                <InputItem
                    label={ 'Lastname' }
                    store={ store.lastName }
                    placeholder={ 'Lastname' }
                    inputProps={{
                        name: 'lastname_field',
                        disabled: store.isSending
                    }}
                />
                <InputItem
                    label={ 'Email' }
                    store={ store.email }
                    placeholder={ 'Email' }
                    inputProps={{
                        name: 'email_field',
                        disabled: store.isSending
                    }}
                />
                <Label>Position</Label>
                <Select
                    className={ styles.positionsSelect }
                    style={{
                        width: '100%'
                    }}
                    defaultValue={ store.userPositionEnum.value }
                    disabled={ store.isSending }
                    onChange={ (v) => store.onPositionChange(v) }
                    store={ store.userPositionEnum }
                >
                    {
                        getPositionsList().map(({ label, value  }) => (
                            <Option key={ value } value={ value }>{ label }</Option>
                        ))
                    }
                </Select>
            </Modal>
        </div>
    )
})
