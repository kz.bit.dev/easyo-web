import {
    action, computed,
    observable,
    runInAction
} from "mobx";
import { message } from "antd";
import FieldStore from "~/code/services/field-store";
import { required, email } from "~/code/services/validators";
import { isValidArr } from "~/code/services/field-store/services";
import { createUser, editUser } from "./services";

export class UserPersonalModalStore {
    private parentStore;
    constructor(parentStore) {
        this.parentStore = parentStore;
    }
    public email = new FieldStore<string>([ required(() => 'Please, enter your email'), email ]);
    public firstName = new FieldStore<string>([ required(() => 'Please, enter firstname') ]);
    public lastName = new FieldStore<string>([ required(() => 'Please, enter lastname') ]);
    public userPositionEnum = new FieldStore<string>([ required(() => 'Please, select position')]);

    @observable
    public isVisible: boolean = false;

    @observable
    public fileList: any[] = [];

    @observable
    public id: string = '';

    @observable
    public isSending: boolean = false;

    @action
    showModal() {
        this.userPositionEnum.set('WAITER');
        this.isVisible = true
    }

    @action
    onPositionChange(v) {
        this.userPositionEnum.set(v)
    }

    @action
    onImageLoaded(file?: { path: string }) {
        this.fileList =
            file ? [{
                uid: '-1',
                name: 'image.png',
                status: 'done',
                url: file.path
            }] : []
    }

    @computed
    public get isEditMode() {
        return !!this.id
    }

    @action
    closeModal() {
        this.isVisible = false;
        this.email.set('');
        this.firstName.set('');
        this.lastName.set('');
        this.userPositionEnum.set('WAITER');
        this.id = null;
        this.fileList = [];
    }

    @action
    async save() {
        if (!isValidArr(this.email, this.firstName, this.lastName, this.userPositionEnum)) return;
        const user = {
            email: this.email.value,
            firstName: this.firstName.value,
            lastName: this.lastName.value,
            userPositionEnum: this.userPositionEnum.value,
            ...( this.fileList && this.fileList.length > 0 ? { imageUrl: this.fileList[0].url }: {})
        };

        this.isSending = true;

        const result = this.isEditMode ? await editUser(this.id, user) : await createUser(user);

        if (!result.error) {
            this.isSending = false;
            message.success(this.isEditMode ? 'User edited' : 'User created');
            this.closeModal();
            this.parentStore.refreshPage();
            return;
        }

        runInAction(() => {
            if (!result.error) return;
            this.isSending = false;
            message.error(result.error.message)
        })
    }

    @action
    setUser(user) {
        this.id = user.id;
        this.email.set(user.email);
        this.firstName.set(user.firstName);
        this.lastName.set(user.lastName);
        this.userPositionEnum.set(user.userPositionEnum);
        this.fileList = user.imageUrl ?
            [{
                uid: '-1',
                name: 'image.png',
                status: 'done',
                url: user.imageUrl
            }]:
            [];
    }
}
