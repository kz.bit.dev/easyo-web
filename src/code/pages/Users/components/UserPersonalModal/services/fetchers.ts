import { User } from "././../models";
import { postWithToken, putWithToken } from "~/code/services/fetchers";
import {apiCheck, Content, form, noThrow, post} from "back-connector";


export function createUser(user: User) {
    return noThrow(apiCheck(postWithToken('/api/users', user)))
}

export function editUser(id: string, user: User) {
    return noThrow(apiCheck(putWithToken('/api/users/' + id, user)))
}

