export function getPositionsList() {
    return [
        {
            label: 'WAITER',
            value: 'WAITER'
        },
        {
            label: 'COOK',
            value: 'COOK'
        },
        {
            label: 'MANAGER',
            value: 'MANAGER'
        }
    ]
}
