import React from "react";
import {action, observable, reaction} from "mobx";
import { Button, message } from "antd";
import {fetchAllUsers, fetchDeleteUser, getUserColumns} from "~/code/pages/Users/services";
import { UserPersonalModalStore } from "./components/UserPersonalModal/UserPersonalModalStore";
import styles from './Users.scss'

export class UsersStore {
    public userPersonalModalStore;
    private parentStore;

    constructor(parentStore) {
        this.parentStore = parentStore
        this.userPersonalModalStore = new UserPersonalModalStore(this);
        this.init()
        reaction(() => this.currentPage, () => {
            this.loadUsers()
        })
    }

    @observable
    public loading: boolean = false;

    @observable
    public users: any[] = [];

    @observable
    public total: number = 1;

    @observable
    public currentPage: number = 0;

    @action
    init() {
        this.loadUsers()
    }

    @action
    onPageChange(value) {
        this.currentPage = value - 1
    }

    @action
    async loadUsers() {
        this.loading = true;
        const result = await fetchAllUsers({ currentPage: this.currentPage, size: 10 });
        this.loading = false;
        if(result.error) {
            message.error('Request failed! Can\'t load users ')
        }

        this.users = result.value.data;
        this.total = result.value.total;

        if(this.users.length === 0 && this.currentPage !== 0) {
            this.currentPage = 0
        }

    }

    @action
    refreshPage() {
        this.loadUsers()
    }

    @action
    createUser() {
        this.userPersonalModalStore.showModal();
    }

    @action
    onEditUser(user) {
        this.userPersonalModalStore.showModal();
        this.userPersonalModalStore.setUser(user)
    }

    @action
    async onDeleteUser(user) {
        const result = await fetchDeleteUser(user.id)
        if(!result.error) {
            message.success(' User deleted');
            this.refreshPage();
            return;
        }
        message.error(result.error.message);
    }

    getGridColumns() {
        const columns = getUserColumns();
        columns.push(
            {
                title: 'Actions',
                field: 'actions',
                render: (text, user) => {
                    return (
                        <>
                            <Button onClick={ () => this.onEditUser(user) }>
                                EDIT
                            </Button>
                            <Button onClick={ () => this.onDeleteUser(user) } className={ styles.deleteButton }>
                                DELETE
                            </Button>
                        </>
                    )
                }
            }
        )
        return columns;
    }
}
