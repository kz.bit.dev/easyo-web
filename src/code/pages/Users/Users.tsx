import React from 'react';
import { observer } from "mobx-react";
import {Button, Grid} from "~/code/components";
import { UsersStore } from "./UsersStore";
import { UserColumn } from "./models";
import { Column}  from "~/code/components/Grid";
import { UserPersonalModal } from "./components";
import styles from './Users.scss'

export default observer(({ store }: { store: UsersStore }) => {
    return (
        <div className={ styles.Users }>
            <div className={ styles.createUserButton }>
                <Button onClick={ () => store.createUser() }> Create user </Button>
            </div>
            <Grid<UserColumn>
                columns={ store.getGridColumns() as Column<UserColumn>[]}
                data={ store.users }
                loading={ store.loading }
                rowKey={ 'id' as keyof UserColumn }
                pagination={{
                    position: 'both',
                    total: store.total,
                    defaultPageSize: 10,
                    current: store.currentPage + 1,
                    onChange: (v) => store.onPageChange(v)
                }}
                scroll={ { x: true } }
            />
            { store.userPersonalModalStore.isVisible && (
                <UserPersonalModal store={ store.userPersonalModalStore } />
            ) }

        </div>
    )
})
