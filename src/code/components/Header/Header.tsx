// @ts-ignore
import React from 'react'
// @ts-ignore
import { observer } from 'mobx-react'
import { Avatar as AntdAvatar, Dropdown as AntdDropdown, Icon, Layout as AntdLayout } from "antd";
import { AuthStore } from "~/code/pages/auth/AuthStore";
import styles from './Header.scss'
import {UserMenu} from "~/code/components/Header/components/UserMenu";
import { MainLayoutStore } from "~/code/components/MainLayout/MainLayoutStore";
import {HeaderNavLink} from "~/code/components/NavLink";


const PageHeader = observer((props: { layoutStore: MainLayoutStore }) => {

    const { layoutStore } = props

    return (
        <AntdLayout.Header className={ styles.Header }>
            <div className={ styles.wrapper }>
                <div className={ styles.logoWrapper }>
                    {
                        AuthStore.isAuthenticated && (
                            <div className={`${ styles.button } ${ styles.menu }`}
                                 onClick={() => { layoutStore.toggleSider() }}>
                                {
                                    layoutStore.isSiderVisible ? <Icon type="menu-fold" /> : <Icon type="menu-unfold" />
                                }
                            </div>
                        )
                    }
                    <HeaderNavLink href="/" className={styles.logo}>
                    </HeaderNavLink>
                </div>
                {
                    AuthStore.isAuthenticated &&
                        <div>
                            <span className={styles.button}>
                                <AntdDropdown overlay={ UserMenu.get() } placement="bottomRight" trigger={['click']}>
                                    <AntdAvatar icon="user" />
                                </AntdDropdown>
                            </span>
                        </div>
                }
            </div>
        </AntdLayout.Header>
    )
});

export { PageHeader as Header }
