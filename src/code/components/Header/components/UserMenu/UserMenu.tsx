import React from "react";
import { computed } from "mobx";
import { Icon, Menu } from "antd";
import { AuthStore } from "~/code/pages/auth/AuthStore";
import { AppStore } from "~/code/AppStore";

const UserMenu = computed(() =>
    <Menu >
        <Menu.Item key="signOut" onClick={ () => AuthStore.logout() }>
            <Icon type="logout" />
            Logout
        </Menu.Item>
    </Menu>
);

export { UserMenu }
