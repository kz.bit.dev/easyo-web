import React from 'react'
import {PaginationConfig} from "antd/lib/pagination";

export type SortOrder = 'asc' | 'desc'

export type Renderer<T> = (value: any, record?: T) => React.ReactNode

export interface Column<T> {
    field: keyof T
    title: React.ReactNode
    titleTooltip?: React.ReactNode
    render?: Renderer<T>
    width?: string | number
    className?: string
    fixed?: boolean
    sortable?: boolean
    sortOrder?: SortOrder
    showTooltip?: boolean
}

export interface ColumnEvents {
    onColumnResize?: (index: number, newWidth: number) => void
    onColumnMove?: (index: number, newIndex: number) => void
}

export interface GridProps<T> extends ColumnEvents {
    emptyDataContent?: React.ReactNode | (() => React.ReactNode)
    columns?: Column<T>[]
    rowKey?: keyof T
    data?: T[]
    scroll?: { x?: boolean | number, y?: boolean | number }
    className?: string
    rowClassName?: string | ((record: T, index: number) => string)
    cellClassName?: string
    loading?: boolean
    isRowSelection?: boolean
    selectedRowKeys?: string[]
    processingRowKeys?: string[]
    onRowClick?: (T, index?: number) => void
    onRowsSelect?: (selectedRowKeys: string[], selectedRows: T[]) => void,
    pagination?: PaginationConfig | false;
}
