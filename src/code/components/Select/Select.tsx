import React from "react";
import { Select } from "antd";
import { observer } from "mobx-react";
import styles from './Select.scss'

export default observer((props) => {
    const { children, store = {}, ...selectProps } = props;
    return (
        <span className={ styles.Select }>
            <Select
                { ...selectProps }
                className={ (props.className ? props.className : '') + ' ' + (store.error === undefined ? '' : ((!!store.value && !store.error) ? '' : styles.error)) }
            >
                {
                    children
                }
            </Select>
            { store.error === undefined ? undefined : ((!!store.value && !store.error) ? null : (
                <div className={ styles.errorMessage }>
                    { store.error }
                </div>
            )) }
        </span>
    )
})
