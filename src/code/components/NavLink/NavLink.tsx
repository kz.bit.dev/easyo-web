import React from "react";
import { observer } from "mobx-react";
import { NavLinkProps } from "~/code/components/NavLink/props";

import page from 'page'
import { observable, runInAction } from "mobx";

const context = observable({
    pathname: ''
})

page((ctx, next) => {
    runInAction(() => {
        context.pathname = ctx.pathname
    })
    next()
})


export const HeaderNavLink = observer((props: NavLinkProps) => (
    <a
        href={props.href}
        className={`${props.className || ''} ${(context.pathname === props.href ? props.activeClassName : '')}`}
        data-prevent-validation={true}
        onClick={props.onClick && props.onClick}
    >
        {props.children}
    </a>
))

