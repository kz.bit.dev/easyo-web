export function getItems() {
    return [
        {
            items: [
                {
                    id: 'dashboard',
                    label: 'Dashboard',
                    icon: 'dashboard',
                    ready: true
                },
                {
                    id: 'users',
                    label: 'Users',
                    icon: 'user',
                    ready: true
                },
                {
                    id: 'tables',
                    label: 'Tables',
                    icon: 'table',
                    ready: true
                },
                {
                    id: 'menus',
                    label: 'Menus',
                    icon: 'menu',
                    ready: true
                },
                {
                    id: 'meals',
                    label: 'Meals',
                    icon: 'coffee',
                    ready: true
                },
                {
                    id: 'orders',
                    label: 'Orders',
                    icon: 'unordered-list',
                    ready: true
                }
            ]
        }
    ];
}
