export interface NavLinkProps {
    href: string
    activeClassName?: string
    children?: any
    className?: string
    onClick?: () => void
}
