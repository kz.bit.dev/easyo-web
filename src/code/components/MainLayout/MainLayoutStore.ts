import { action, observable } from "mobx";

class MainLayoutStore {
    @observable
    private parentStore;

    constructor(parentStore) {
        this.parentStore = parentStore;
    }

    @observable
    public isSiderVisible: boolean = true;

    @observable
    public openMainMenuKeys: string[] = ['sub_menu_item']

    @action
    public openSubMenu(openKeys: string[]) {
        this.openMainMenuKeys = openKeys.filter((key) => !this.openMainMenuKeys.includes(key))
    }

    @action
    public toggleSider() {

        this.isSiderVisible = !this.isSiderVisible;

        setTimeout(this.triggerResizeEvent, 300)
    }

    private triggerResizeEvent() {
        const event = document.createEvent('HTMLEvents');
        event.initEvent('resize', true, false);
        window.dispatchEvent(event)
    }
}


export { MainLayoutStore }
