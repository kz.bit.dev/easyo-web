import React from 'react';
import { Header } from '~/code/components/Header';
import { MainLayoutStore } from './MainLayoutStore';
import { AuthStore } from '~/code/pages/auth/AuthStore';
import { Sider } from '~/code/components/Sider';
import { Card } from 'antd';
import { PageTitle } from '~/code/components/PageTitle';
import styles from './MainLayout.scss'


const MainLayout = ({ mainLayoutStore, children, title } : { mainLayoutStore: MainLayoutStore, children?: JSX.Element, title?: string } ) => {
    return (
        <div className={ styles.MainLayout }>
            <Header layoutStore={ mainLayoutStore } />
            <div className={ styles.wrapper }>
                <div className={ styles.mainWrapper }>
                    <div>
                        <div className={`${styles.contentWrapper} ${ mainLayoutStore.isSiderVisible ? styles.withSider : ''}`}>
                            {
                                AuthStore.isAuthenticated && <Sider layoutStore={ mainLayoutStore } />
                            }
                            <div className={ styles.pageContentWrapper }>
                                <PageTitle title={ title } />
                                <Card data-semantic-id='page' className={ styles.pageContent } bordered={ false }>
                                    <div>
                                        { children }
                                    </div>
                                </Card>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { MainLayout }
