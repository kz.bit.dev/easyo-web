import React from 'react'

import { observer } from 'mobx-react'

import {Icon, Menu} from 'antd'

import styles from './MainMenu.scss'
import { MainLayoutStore } from "~/code/components/MainLayout/MainLayoutStore";
import { HeaderNavLink } from "~/code/components/NavLink/NavLink";
import { getItems } from "~/code/components/NavLink/services";

const SubMenu = Menu.SubMenu
const MenuItemGroup = Menu.ItemGroup
const MenuItem = Menu.Item

const MainMenu = observer((props: { layoutStore: MainLayoutStore }) => {
    const { layoutStore } = props;
    const renderGroup = (group, index: number) => {
        return (
            <MenuItemGroup key={`group_${index}`}>
                {group.items.map((item, i) => renderItem(item))}
            </MenuItemGroup>
        )
    }

    const renderItem = (item) => {
        const icon = item.icon ? <Icon type={ item.icon }/> : null

        if (!item.items) {
            return renderSubItem(item.id, item.ready, item.label,  icon)
        }

        return (
            <SubMenu key={`sub_menu_${item.id}`} title={ <span>{ icon }{ item.label }</span> }>
                {item.items.map((subItem) => renderSubItem(subItem, item.ready.includes(subItem), item.label,null, item.id))}
            </SubMenu>
        )
    }

    const onClick = () => {
        const shouldHideSider = window.matchMedia('(max-width: 980px)').matches
        if (shouldHideSider && layoutStore.isSiderVisible) {
            layoutStore.toggleSider()
        }
    }

    const renderSubItem = (itemId: string, isReady: boolean, label: string, icon?: JSX.Element, parentId?: string) => {
        const href = `/${parentId ? (parentId + '/') : ''}${itemId}`

        return (
            <MenuItem key={`item_${itemId}`} >
                <HeaderNavLink href={href} activeClassName={styles.active} className={!isReady ? styles.notReady : ''} onClick={onClick}>{icon || null}{ label }</HeaderNavLink>
            </MenuItem>
        )

    }


    return (
        <div>
            <Menu mode="inline"
                  className={styles.mainMenu}
                  openKeys={[ ...layoutStore.openMainMenuKeys ]}
                  onOpenChange={(keys: string[]) => { layoutStore.openSubMenu(keys) }}
                  selectedKeys={['item_1']}>
                { getItems().map((item, i) => renderGroup(item, i)) }
            </Menu>
        </div>
    )
})

export default MainMenu
