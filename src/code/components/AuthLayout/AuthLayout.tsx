import React from 'react';
import styles from './AuthLayout.scss'

export default function (props) {
    return (
        <div className={ styles.AuthLayout }>
            <div className={ styles.container }>
                <div className={ styles.logo }></div>
                <div className={ styles.content }>{ props.children }</div>
            </div>
        </div>
    )
}
