import React from 'react'
import { observer } from 'mobx-react'
import { InputProps } from 'antd/lib/input'
import { PopoverProps } from 'antd/lib/popover'
import { Input, Popover } from '~/code/components'
import FieldStore from '~/code/services/field-store'
import { isNumber } from '~/code/services';

interface FieldInputProps extends InputProps {
    store: FieldStore<string>
    popover?: PopoverProps
    trigger?: 'onChange' | 'onBlur' | 'onKeyUp'
    isNumeric?: boolean
    focused?: boolean
}

const FieldInput = observer((props: FieldInputProps) => {

    const { popover, store, isNumeric, trigger: triggerProp, ...rest } = props
    const { value } = store

    const trigger = triggerProp || 'onBlur'
    const validateOnChange = trigger === 'onChange'

    const onBlur = trigger !== 'onBlur' ? undefined : e => {
        if (e.relatedTarget && e.relatedTarget.dataset.preventValidation) return
        store.validate()
    }
    const onKeyUp = trigger !== 'onKeyUp' ? undefined : store.validate

    const handleValueChange = (newValue: string) => {

        if (newValue.length && isNumeric && !isNumber(newValue)) {
            return
        }

        store.set(newValue, validateOnChange)
    }

    const input = <Input
        value={value}
        onChange={(e) => handleValueChange(e.target.value)}
        onBlur={onBlur}
        onKeyUp={onKeyUp}
        {...rest}
    />

    return popover
        ? <Popover {...popover}>
            {input}
        </Popover>
        : input
})

export default FieldInput
