import React from "react";
import { DatePicker } from "antd";
import { observer } from "mobx-react";
import styles from './DatePicker.scss'

export default observer((props) => {
    const { children, store, ...selectProps } = props;
    return (
        <span className={ styles.DatePicker }>
            <DatePicker
                { ...selectProps }
                value={ store.value }
                className={ (props.className ? props.className : '') + ' ' + (store.error === undefined ? '' : ((!!store.value && !store.error) ? '' : styles.error)) }
            >
                {
                    children
                }
            </DatePicker>
            { store.error === undefined ? undefined : ((!!store.value && !store.error) ? null : (
                <div className={ styles.errorMessage }>
                    { store.error }
                </div>
            )) }
        </span>
    )
})
