import React from "react";

export default function (props) {
    return (
        <div className="ant-form-item-label">
            <label>{ props.children }</label>
        </div>
    )
}
