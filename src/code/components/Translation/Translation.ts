export type Language = 'en'

export interface Translation<T> {
    (): T
    add(lang: Language, value: T): Translation<T>
}
