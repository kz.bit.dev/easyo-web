import React from "react";
import { Helmet } from 'react-helmet'
import styles from './PageTitle.scss'

const PageTitle = ({ title }) => {
    return (
        <div className={ styles.PageTitle }>
            <Helmet>
                <title>
                    { title }
                </title>
                <meta charSet="utf-8" />
            </Helmet>
            { title }
        </div>
    )
};

export { PageTitle }
