import React from "react";
import { observer } from "mobx-react";
import { Layout } from 'antd'
import styles from './Sider.scss'
import { MainLayoutStore } from "~/code/components/MainLayout/MainLayoutStore";
import MainMenu from "~/code/components/MainMenu/MainMenu";

const Sider = observer((props: { layoutStore: MainLayoutStore }) => {

    const { layoutStore } = props
    return (
        <Layout.Sider className={`${ styles.Sider } ${layoutStore.isSiderVisible ? styles.visible : styles.hidden}`}>
            <MainMenu layoutStore={ layoutStore }/>
        </Layout.Sider>
    )
})

export { Sider }
