import {apiCheck, Content, ContentType, del, get, noThrow, post, put} from "back-connector";
import { getUserToken } from "~/code/pages/auth/services";
import { AuthStore } from "~/code/pages/auth/AuthStore";


export function formDataContent(data) {
    return new Content(
        '',
        data === undefined ? undefined : data
    )
}

export async function getWithToken<T>(url: string, data?: ContentType) {
    const headers: string[][] = [];
    const token = getUserToken();
    if (token) {
        headers.push(['Authorization', token])
    }
    const response = await get<T>(url, data, headers);

    if (!token || response.status === 401) {
        AuthStore.logout()
    }

    return response;
}

export async function postWithToken<T>(url: string, data?: ContentType, _headers?: any[][]) {
    const headers: string[][] = _headers ? _headers : [];
    const token = getUserToken();
    if (token) {
        headers.push(['Authorization', token])
    }
    const response = await post<T>(url, data, headers);

    if (!token || response.status === 401) {
        AuthStore.logout()
    }

    return response;
}

export async function putWithToken<T>(url: string, data?: ContentType) {
    const headers: string[][] = [];
    const token = getUserToken();
    if (token) {
        headers.push(['Authorization', token])
    }
    const response = await put<T>(url, data, headers);

    if (!token || response.status === 401) {
        AuthStore.logout()
    }

    return response;
}

export async function delWithToken<T>(url: string, data?: ContentType) {
    const headers: string[][] = [];
    const token = getUserToken();
    if (token) {
        headers.push(['Authorization', token])
    }
    const response = await del<T>(url, data, headers);

    if (!token || response.status === 401) {
        AuthStore.logout()
    }

    return response;
}

export async function uploadFile(file) {
    const formData = new FormData();
    formData.append('file', file)


    return noThrow(
        apiCheck<{ path: string }>(
            postWithToken(`api/storage/upload`, formDataContent(formData))
        ))
}
