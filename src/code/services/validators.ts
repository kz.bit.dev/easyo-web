import { MessageType, Rule } from '~/code/models';

export function isNumber(value: string) {
    return !isNaN(parseFloat(value)) && isFinite(+value)
}

export function create<T>(rule: Rule<T>, message: MessageType) {

    const result = (newMessage: MessageType) => ({
        rule,
        message: newMessage
    });

    result.rule = rule;
    result.message = message;

    return result
}

export const required = create((value) => !!value, () => 'Required field');

const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

export const email = create<string>((value) => !value || emailRegex.test(value), () => 'Incorrect email format');

export const numeric = create<string>((value) => !value || isNumber(value), 'Incorrect number format');

export const positiveNumeric = create<string>((value) => !value || (isNumber(value) &&  parseFloat(value) >= 0), 'Incorrect number format');
