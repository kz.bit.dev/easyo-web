export type Rule<T> = (value: T) => boolean

export type MessageType = string | (() => string)

export interface Validation<T> {
    rule: Rule<T>
    message: MessageType
}
