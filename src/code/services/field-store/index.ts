import { action, computed, observable } from 'mobx'

import { MessageType, Validation } from './models'

export default class Index<T> {

    constructor(private validations: Validation<T>[] = null, private onChange?: (v: T) => any) { }

    @observable
    public value: T

    @computed
    public get error() {
        return typeof this._error === 'function' ? this._error() : this._error
    }

    @action
    public set(value: T, validate = false) {

        this._error = undefined

        this.value = value

        if (validate) {
            this.validate()
        }

        this.onChange && this.onChange(value)
    }

    @action.bound
    public validate() {

        if (this.validations) {

            for (const validation of this.validations) {
                if (!validation.rule(this.value)) {
                    this._error = validation.message
                    return
                }
            }
        }

        this._error = null
    }

    @action
    public isValid() {

        if (this._error === undefined) {
            this.validate()
        }

        return this._error === null
    }

    @observable
    /* tslint:disable */
    private _error: MessageType
    /* tslint:enable */
}
