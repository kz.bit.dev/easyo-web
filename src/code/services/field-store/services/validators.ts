import FieldStore from './../index'
export function isValidArr(...fields: FieldStore<any>[]) {
    return fields.reduce((result, store) => store.isValid() && result, true)
}
