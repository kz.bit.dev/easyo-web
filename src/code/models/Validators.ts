import { MessageType } from "./MessageType";
import { Rule } from "./Rule";

export interface Validators<T> {
    rule: Rule<T>
    message: MessageType
}
