import React from 'react'
import { render } from 'react-dom'
import page from 'page'

import { App } from './App'
import { AppStore } from './AppStore'
import './startup/Router'

// initialize the router
page()

render(<App store={AppStore} />, document.getElementById('ease-content'))
