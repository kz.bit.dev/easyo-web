import page from 'page'
import { AppStore as store } from '../../AppStore'
import qs from 'qs'

let path: string;
page('*', (ctx, next) => {
    path = ctx.path;
    ctx.query = qs.parse(ctx.querystring);
    next()
});

page('/', () => store.redirectToDashboard());
page('/dashboard', () => store.showDashbordPage());
page('/users', () => store.showUsersPage());
page('/tables', () => store.showTablesPage());
page('/menus', () => store.showMenusPage());
page('/orders/create', () => store.showOrdersPersonalPage());
page('/orders/:id', (cnx) => store.showOrdersPersonalPage(cnx.params.id));
page('/orders', () => store.showOrdersPage());
page('/meals', () => store.showMealsPage());
page('/login', () => store.showLogin());
