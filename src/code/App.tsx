import React from 'react'
import { AppStore } from '~/code/AppStore'
import { observer } from 'mobx-react'
import '~/assets/styles/main.scss'

const App = observer(({ store }: { store: typeof AppStore }) => {
    const Page = observer(() => store.currentPage())
    return <>
        <Page/>
    </>
})

export { App }
