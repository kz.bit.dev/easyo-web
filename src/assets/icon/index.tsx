import React from 'react'

import { Icon } from 'antd'
import { IconProps } from 'antd/lib/icon'

export const LoadingIcon = (props: IconProps) => <Icon type="loading" {...props} />
