'use strict'

const path = require('path')

const incstr = require('incstr')

const webpack = require('webpack')

const MiniCssExtractPlugin = require('mini-css-extract-plugin') //later replace ExtractTextPlugin by this plugin

const ExtractTextPlugin = require('extract-text-webpack-plugin')

const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')

const tsImportPluginFactory = require('ts-import-plugin')

const OptimizeCssnanoPlugin = require('@intervolga/optimize-cssnano-plugin')

const HtmlWebpackPlugin = require('html-webpack-plugin')

const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

const env = process.env.NODE_ENV || 'development'

const production = env === 'production'


module.exports = {
    mode: env,
    devtool: production ? undefined : 'source-map',
    entry: {
        'app': './src/code/index.tsx'
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js', '.css', '.scss'],
        mainFields: ['main'],
        alias: {
            '~': path.resolve(__dirname, 'src/'),
            '#': path.resolve(__dirname, 'src/assets/'),
            '@': path.resolve(__dirname, 'src/code/')
        }
    },
    node: { fs: 'empty' },
    module: {
        rules: [
			{
				test: /\.tsx?$/,
				loader: 'ts-loader',
				options: {
                    transpileOnly: !production,
					experimentalWatchApi: true,
					getCustomTransformers: () => ({
						before: [tsImportPluginFactory({ style: 'css' })]
					})
				},
				exclude: /node_modules/
			},
            {
                test: /\.js$/,
                enforce: 'pre',
                loader: 'source-map-loader',
                exclude: [
                    path.resolve(__dirname, 'node_modules/mutationobserver-shim')
                ]
            },
            {
                test: /\.css$/,
                include: [
                    path.resolve(__dirname, 'src')
                ],
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                modules: true,
                                camelCase: true,
                                ident: 'postcss',
                                localIdentName: production ? undefined : '[path][name]_[local]',
                                getLocalIdent: !production ? undefined : (function () {
                                    const cache = {}
                                    const nextId = incstr.idGenerator({
                                        alphabet: 'abcdefghijklmnopqrstuvwxyz0123456789_-'
                                    })
                                    return (loaderContext, localIdentName, localName, options) => {
                                        const key = `${loaderContext.resourcePath}!${localName}`
                                        return cache[key] || (cache[key] = `_${nextId()}`)
                                    }
                                })(),
                                importLoaders: 1
                            }
                        },
                        'postcss-loader'
                    ]
                })
            },
            {
                test: /.css$/,
                include: [
                    path.resolve(__dirname, 'node_modules')
                ],
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader'
                        }
                    ]
                })
            },
            { test: /\.scss$/, use: [
                    { loader: "style-loader" },  // to inject the result into the DOM as a style block
                    // { loader: "css-modules-typescript-loader"},  // to generate a .d.ts module next to the .scss file (also requires a declaration.d.ts with "declare modules '*.scss';" in it to tell TypeScript that "import styles from './styles.scss';" means to load the module "./styles.scss.d.td")
                    { loader: "css-loader", options: { modules: true } },  // to convert the resulting CSS to Javascript to be bundled (modules:true to rename CSS classes in output to cryptic identifiers, except if wrapped in a :global(...) pseudo class)
                    { loader: "sass-loader" },  // to convert SASS to CSS
                    // NOTE: The first build after adding/removing/renaming CSS classes fails, since the newly generated .d.ts typescript module is picked up only later
                    {
                        loader: 'sass-resources-loader',
                        options: {
                            resources: [
                                './src/assets/styles/vars.scss',
                                './src/assets/styles/mixins.scss'
                            ]
                        }
                    }
            ] },
            {
                test: /\.(svg|png)/,
                include: [
                    path.resolve(__dirname, 'src/assets')
                ],
                use: [{
                    loader: 'url-loader',
                    options: {
                        limit: 4096, // Convert images < 8kb to base64 strings
                        name: 'img/[name].[ext]'
                    }
                }]
            },
            {
                test: /\.(png)/,
                include: [
                    path.resolve(__dirname, 'node_modules')
                ],
                use: [{
                    loader: 'url-loader',
                    options: {
                        name: 'img/[name].[ext]'
                    }
                }]
            },
            {
                test: /.*/,
                include: [
                    path.resolve(__dirname, 'src/assets/fonts')
                ],
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: 'fonts/[name].[ext]'
                    }
                }]
            }
        ]
    },
    plugins: [
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        new ExtractTextPlugin({
            filename: production || true ? '[name].[chunkhash].css' : '[name].css'
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'src/assets/index.html')
        }),
        new ForkTsCheckerWebpackPlugin()
    ],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: production || true ? '[name].[chunkhash].js' : '[name].js',
        publicPath: '/'
    },
    optimization: production || true ? {
        runtimeChunk: 'single',
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all'
                }
            }
        }
    } : {
		removeAvailableModules: false,
		removeEmptyChunks: false,
		splitChunks: false
	},
    devServer: {
        contentBase: path.resolve(__dirname, 'dist'),
        historyApiFallback: true,
        port: 8080,
        proxy:
            [{
                context: '/api',
                target: 'https://easyo.herokuapp.com',
                secure: false,
                changeOrigin: true,
                logLevel: 'debug'
            }]
    }
}


if (production) {

    module.exports.module.rules.unshift(
        {
            test: /\.tsx?$/,
            enforce: 'pre',
            loader: 'tslint-loader',
            options: {
                formatter: 'prose',
                emitErrors: true
            },
            exclude: [
                /node_modules/,
                path.resolve(__dirname, 'src/assets')
            ]
        }
    )

    module.exports.plugins.push(
        new webpack.HashedModuleIdsPlugin()
    )

    module.exports.plugins.push(
        new BundleAnalyzerPlugin({
            analyzerMode: 'static',
            defaultSizes: 'gzip',
            openAnalyzer: false,
            logLevel: 'warn',
            reportFilename: path.resolve(__dirname, 'bundle-report.html')
        })
    )

    module.exports.plugins.push(
        new OptimizeCssnanoPlugin({
            cssnanoOptions: {
                preset: ['default', {
                    discardComments: {
                        removeAll: true
                    }
                }]
            }
        })
    )
}
