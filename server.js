const express = require('express');
const path = require('path');
const port = process.env.PORT || 8080;
const app = express();
const { createProxyMiddleware } = require('http-proxy-middleware');

// the __dirname is the current directory from where the script is running
app.use(express.static(path.resolve(__dirname, 'dist')));
app.use('/api', createProxyMiddleware({ target: 'https://easyo.herokuapp.com', changeOrigin: true }));

// send the user to index html page inspite of the url
app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'dist/index.html'));
});

app.listen(port);
